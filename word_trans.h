#ifndef WORD_TRANS_H
#define WORD_TRANS_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QMap>
#include <QString>
#include <QTextEdit>
#include<QStringList>


class word_trans : public QObject
{
    Q_OBJECT
public:
    explicit word_trans(QObject *parent = nullptr);

    void CreateDict(QMap<QString, QString>  *myDict,QMap<QString, QString>  *myDict_chi_en);


    QString translate(QString word);
    QString translate_chi_en(QString chi,bool mohu);
    QString translate_en_chi(QString word,bool mohu);
    QString entochi(QString word,int leftlen);
    QStringList trans_entochi(QString word,int leftlen ,int need);

signals:


private:
    QMap<QString, QString> mapDict;
    QMap<QString, QString> mapDict_chi_en;
    QString query;
    QString exepath;

};

#endif // WORD_TRANS_H
