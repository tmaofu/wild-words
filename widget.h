#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include<qfiledialog.h>
//#include<QAxObject>
#include<qdebug.h>
#include<words.h>
#include<qtimer.h>
#include <QCloseEvent>
#include<QStringListModel>
#include<QDesktopServices>
#include<QMessageBox>
#include <QDesktopServices>

//#include<QtAndroidExtras/QAndroidJniObject>
//#include<QtAndroidExtras/QtAndroid>
//#include<QtAndroidExtras/QAndroidJniEnvironment>

#include<QResource>
#include <QButtonGroup>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
private:
    //单词 播放,翻译,显示图片
    void wild_word_start(QString word);
    void wild_word_start_start();
private:

    //上一个.下一个.点击列表 重复代码
    void wild_word_helpStart();

    //显示网页
    void show_wangye(QString wanzhi,QString wanzi2,QString wanzi3,QString wanzhi4);


    //显示当前单词位置,进度
    void show_currentpos();

    //保存单词数据
    bool savewords_data();//追加
    void savewords_data_fengci();//分次

    //导入单词数据
    void getwords_data(QString word_filename);
    //触发导入数据的函数 相同代码
    void helpdaoru();

    //单词回顾功能
    bool danchi_huigu();

    //更新历史数据界面  (以历史数据文件(txt)为准
    void genxing_lishishuju();

    void deletelishishuju(QModelIndex index);
    QString  lishishuju_help(QString str);
    void editlishishuju(QModelIndex index);
    void daorulishishuju(QModelIndex index);
    //划分获取历史文件名
    QString getlishifilename(QModelIndex index);

    //删除文件
    bool DeleteFileOrFolder( QString strPath);

    //得到图片
    void getwordpic(bool newpic);
    void showwordpic(bool success);

    //数据目录
    int setdatapxath();

    //背景
    void showbeijingtu();
    void resizeEvent(QResizeEvent * ev);

    //导出文件
    bool copyDirectoryFiles(const QString &fromDir, const QString &toDir, bool coverFileIfExist);
    bool cepyfiletopath(QString filename,QString path);

    //文件修改
    void showcureditfile(QString cur_xiugenfilename);


    //导入app数据
    bool daoru_appdata();

    //权限
    //bool requestPermission(QString name);

    //单词测试
    void danchi_cheshi();
    void cheshi_showwordpic(QLabel *lab,QString filename);
    private slots:
    void radioButton_click(int);
    private :

    //动态单词,音节划分
    void Dynamicword(QString word);
    void dynamicword(QString word="");

    //循环播放列表
    void xunhuan_bofan_liepiao();

private ://android 无响应 ,槽要写好
private slots:

    void on_pushButton_next_clicked();

    void on_pushButton_wordslist_clicked();

    void on_listView_clicked(const QModelIndex &index);

    void on_pushButton_clicked();

    void on_pushButton_last_clicked();

    void on_openfile_bytxt_clicked();//这个函数可合并

//    void on_danchibofan_jiange_valueChanged(double arg1);//bug 部件提升后,失效

    void on_danchibofan_jiange_valueChanged(int arg1);

    void on_spinBox_huigu_valueChanged(int arg1);

    void on_pushButton_lookshiyi_clicked();

    void on_listView_huigu_clicked(const QModelIndex &index);

    void on_pushButton_chengchiben_clicked();

    void on_listView_lishi_shuju_doubleClicked(const QModelIndex &index);

    void on_listView_huigu_shiyi_doubleClicked(const QModelIndex &index);

    void on_pushButton_dele_jushuh_clicked();

    void on_pushButton_dele_oushuh_clicked();

    void on_pushButton_daoru_text_clicked();

    void on_pushButton_textedit_celer_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_change_pic1_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();


    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_3_clicked();


    void on_pushButton_8_clicked();

    void on_spinBox_valueChanged(int arg1);

    void on_pushButton_9_clicked();

    void on_pushButton_10_clicked();

    void on_pushButton_11_clicked();


    void on_pushButton_12_clicked();

    void on_pushButton_13_clicked();

    void on_tabWidget_2_currentChanged(int index);

    void on_pushButton_14_clicked();

    void on_pushButton_15_clicked();


    void on_lineEdit_chidian_inp_textChanged(const QString &arg1);

    void on_spinBox_dongtai_msec_valueChanged(int arg1);

    void on_pushButton_16_clicked();

    void on_checkBox_stop_xunhuan_currlist_stateChanged(int arg1);

private:
    Ui::Widget *ui;

    //excel文件
    //QString excfilename;

    //单词类
    words wordclass;
    //单词strlist
    QStringList wordslist;
    //单词列表模型
    QStringListModel wordlist_model;

    QString exepath;
    //单词发音mp3存放路径
    QString word_mp3_path;
    //单词数据存放文件,数据追加
    QString word_data_finame;
    //单词数据,分次 存放路径
    QString word_data_path;
    //生词本文件
    QString shengchi_filename;
    //历史记录文件
    QString lishi_jilu_filename;
    //导出app数据存放路径
    QString daochupath;

    //当前需要修改的文件
    QString crueidtfile;
    //当前单词位置
    int current_word_pos=0;
    //词典本文件
    QString chidianbenfilename;


    //开始的衔接参数
    //QStringList wild_word_chanshu; 不好
    //int intshowword=0,intmp3=0;

    QString curmp3filename,showtext,curword,curfanyi;
    bool curshow_wordfanyi=true;


    //单词播放间隔
    int danchi_jiange=1500;

    //单词回顾
    int danchi_huigu_chishu=8;
    QStringList huigulist,huigu_fanyi_list;
    QStringListModel huigu_model,huigu_fanyi_model;

    //防止快速点击
    QTimer timer_dianji;

    //更新历史数据界面
    QStringList lishishuju_list;
    QStringListModel lishishuju_list_medel;

    //
    QTimer timerhelpstart;

    //图片
    QString picepath;
    QString picturefilename;
    QImage beforeimg;

    //背景
    QLabel*m_lblBg;
    QLabel*m_lblBg_2;
    QString beijintuname;

    //单词测试
    int cheshi_currpos=0;
    QButtonGroup buttgroup;
    bool bo_huigu=false;

    //动态单词
    //int dongtai_word_sec=1000;
    int dongtai_currpos=0;
    QStringList dongtai_list;
    QTimer dongtai_timer,dongtai_timer_jiange;

    //循环播放列表
    QTimer timer_xunhuan_bofan_liebiao;

//protected:
//     void closeEvent(QCloseEvent *event);

};
#endif // WIDGET_H
