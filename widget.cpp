#include "widget.h"
#include "ui_widget.h"
#include<QDir>
#include<qfileinfo.h>
#include<QPainter>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    this->setWindowTitle("MaofU狂野记单词");

    //qt.network.ssl: QSslSocket::connectToHostEncrypted: TLS initialization failed
    // oppssl wen文件不用放到 debug目录下

    //"E MediaPlayerNative: error (1, -2147483648)
    //E MediaPlayer: Error (1,-2147483648)"
    //闪退
    //原因是没联网,mp3不是返回的数据,MediaPlayerNative 无法播放


    //使用[] 先判断是否超出范围,众多bug来源;

//路径
    //QString exepath;
    int datapath=setdatapxath();
    if(datapath==1)
        exepath=QDir().currentPath();//此路径android上是系统目录,不可访问
    else
    {
        //forandroid
        exepath="/storage/emulated/0";//鸿蒙创建失败,默认没有开启存储权限
    }


    //
    QDir direxe;
    if(direxe.exists(exepath)==false)
    {
        qDebug()<<"不存在系统路径";
        ui->label_show->setText("不存在系统路径");
    }

    //datapath
    exepath=exepath+"/wild_word狂野单词/Data";
    ui->label_datapath->setText("数据存放路径:\n"+exepath);
    if(direxe.exists(exepath))
    {
        qDebug()<<"存在data目录";
        ui->label_show->setText("存在data目录");
    }
    else
    {
        qDebug()<<"不存在data目录";
        ui->label_show->setText("不存在data目录");

        bool bo=direxe.mkpath(exepath);
        qDebug()<<QString("data目录创建 %1").arg(bo);
        ui->label_show->setText(QString("data目录创建 %1").arg(bo));
    }

    //单词发音mp3存放路径
    word_mp3_path=exepath+"/word_mp3";
    QDir dir;
    if(dir.exists(word_mp3_path)==false)
    {
        dir.mkpath(word_mp3_path);

    }
    picepath=exepath+"/word_pic";
    if(dir.exists(picepath)==false)
    {
        dir.mkpath(picepath);

    }
    //保存单词文件
    word_data_finame=exepath+"/words.txt";
    QFileInfo info(word_data_finame);
    if(info.isFile()==0)
    {
        QFile file(word_data_finame);
        file.open( QIODevice::ReadWrite | QIODevice::Text );
               file.close();
    }

     //单词数据,分次 存放路径
    word_data_path=exepath+"/word_dada";
    if(dir.exists(word_data_path)==false)
        dir.mkpath(word_data_path);

    //生词本文件
    shengchi_filename=exepath+"/shengchiben.txt";
    QFileInfo info1(shengchi_filename);
    if(info1.isFile()==0)
    {
        QFile file(shengchi_filename);
        file.open( QIODevice::ReadWrite | QIODevice::Text );
               file.close();
    }
    //历史记录文件
    lishi_jilu_filename=exepath+"/历史记录.txt";
    //词典本文件
    chidianbenfilename=exepath+"/词典本.txt";

    //导出路径
    daochupath="/storage/emulated/0/wild_word狂野单词导出数据";
    if(dir.exists(daochupath)==0)
        dir.mkpath(daochupath);


    //获取数据
    getwords_data(word_data_finame);

    //网页,单词图片

    //有道词典

    //单词列表
    ui->dockWidget->setVisible(false);

    //单词表
    ui->listView->setEditTriggers(QAbstractItemView::NoEditTriggers);//不可编辑
    ui->listView->show();
    ui->dockWidget->show();
    //回顾
    ui->listView_huigu->show();
    ui->listView_huigu->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listView_huigu_shiyi->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listView_huigu_shiyi->hide();
    //历史记录
    ui->listView_lishi_shuju->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->listView_lishi_shuju->show();
    this->genxing_lishishuju();
    //当前页
    ui->tabWidget->setCurrentWidget(ui->tab);
    ui->tabWidget_2->setCurrentWidget(ui->tab_3);

    //单词回顾 字体一致
    ui->listView_huigu->setFont(QFont("",15));
    ui->listView_huigu_shiyi->setFont(QFont("",15));
    //
    ui->textBrowser->setMinimumHeight(180);

    //动态单词
//    QFont font;font.setPointSize(24);
//    ui->label_dongtai_word->setFont(font);
    ui->spinBox_dongtai_msec->setMinimum(1);
    ui->spinBox_dongtai_msec->setValue(200);

    //单词测试

    buttgroup.addButton(ui->radioButton,0);
    buttgroup.addButton(ui->radioButton_2,1);
    buttgroup.addButton(ui->radioButton_3,2);
    buttgroup.addButton(ui->radioButton_4,3);
    buttgroup.setExclusive ( true );

    connect(&buttgroup,SIGNAL(buttonClicked(int)), this, SLOT(radioButton_click(int)));

    //播放单词列表
    ui->spinBox_bofang_liebao->setValue(4000);



//信号

    //防止快速点击
    connect(&timer_dianji,&QTimer::timeout,[=](){timer_dianji.stop();ui->pushButton_last->setEnabled(true);ui->pushButton_next->setEnabled(true);ui->pushButton->setEnabled(true);ui->listView->setEnabled(true);ui->listView_huigu->setEnabled(true);});

    //准备好了
    //此法在android上有bug,会闪退F libc    : Fatal signal 11 (SIGSEGV), code 1 (SEGV_MAPERR), fault addr 0xc in tid 6703 (qtMainLoopThrea), pid 6660 (mple.Wild_words)
    //所以我想批量生成mp3先
    //查阅知是 qt15的问题 ,
//    connect(&wordclass,&words::signal_getword_mp3,[=](){timerhelpstart.start(300);});
//    connect(&timerhelpstart,&QTimer::timeout,[=](){timerhelpstart.stop();this->wild_word_start_start();});

    //
    connect(&wordclass,&words::signal_getword_mp3,[=](){this->wild_word_start_start();});

    //批量音频
    connect(&wordclass,&words::singal_getmp3list,[=](){ui->label_show->setText("音频导入成功!");});

    //图片
    connect(&wordclass,&words::singal_getwordpic,[=](bool success){this->showwordpic(success);});

    //动态单词
    connect(&dongtai_timer,&QTimer::timeout,[=](){dongtai_timer.stop();dynamicword();});
    connect(&dongtai_timer_jiange,&QTimer::timeout,[=](){dongtai_timer_jiange.stop();dynamicword();});

    //循环播放列表
    connect(&timer_xunhuan_bofan_liebiao,&QTimer::timeout,[=](){timer_xunhuan_bofan_liebiao.stop();xunhuan_bofan_liepiao();});

    //背景
    beijintuname=":/pic1";
    m_lblBg = new QLabel(ui->tabWidget);
    m_lblBg ->setPixmap(QPixmap(beijintuname));
    m_lblBg ->setScaledContents(true);
    m_lblBg->lower();

    ui->tabWidget->setStyleSheet("background-color: rgba(255, 255, 255, 80);");

    //导出数据
    ui->pushButton_6->setEnabled(false);


// 实现不了
//   beijintuname=":/pic1";
//   this->showbeijingtu();


}


Widget::~Widget()
{
    delete ui;
}


void Widget::resizeEvent(QResizeEvent * ev)
{


    m_lblBg ->setPixmap(QPixmap(beijintuname));
    m_lblBg->resize(ui->tabWidget->size());


}


void Widget::wild_word_start(QString word)
{
    qDebug()<<"wild_word_start";

    //准备工作
    //注意::::单词不能包含特殊字符  pyramid\r.mp3  否则查不到单词,音频文件也不能打开输入
    //"void words::ReplyFinished(QNetworkReply *reply) 打开音频输入文件错误 D:/QTproject/build-Wild_words-Desktop_Qt_5_9_9_MinGW_32bit-Debug/Dada/word_mp3/pyramid\r.mp3"
    //注意::::单词不能包含特殊字符  pyramid\r.mp3  否则查不到单词,音频文件也不能打开输入
    //注意::::单词不能包含特殊字符  pyramid\r.mp3  否则查不到单词,音频文件也不能打开输入
    //注意::::单词不能包含特殊字符  pyramid\r.mp3  否则查不到单词,音频文件也不能打开输入

    word.remove("\n");
    word.remove("\n\r");
    word.remove("\r\n");
    word.remove("\r");

       //单词为空
       if(word.isEmpty())
       {
           ui->label_show->setText("单词为空");
           return;
       }

     //图片
    ui->pushButton_change_pic1->setEnabled(false);
    this->getwordpic(false);

    //单词字典
       QString wordtrans;
       if(wordclass.IsEnglish(word))
            wordtrans= wordclass.getword_trans(word);
       else
           wordtrans=wordclass.getword_trans_chi_en(word,true);

    //单词发音
    QString mp3filename=word_mp3_path+QString("/%1.mp3").arg(word);


        //检查是否已经存在mp3
    QFileInfo dir(mp3filename);
    if(dir.isFile()==false)
    {
        qDebug()<<"音频库中没有此音频,网络下载";
        ui->label_show->setText("音频库中没有此音频,网络下载");
        wordclass.getword_mp3(word,mp3filename);

    }
    else
    {
        qDebug()<<"已经存在mp3";
    }


    showtext=(QString (word+"\n"+wordtrans));
    curword=word;
    curfanyi=wordtrans;

    //此方式不好(用常量 )
    curmp3filename=(mp3filename);


    //已经存在音频,直接开始
    if(dir.exists(mp3filename)==true)
        wild_word_start_start();


}

void Widget::wild_word_start_start()
{
    qDebug()<<"wild_word_start_start";

    //开始
    //ui text
    if(curshow_wordfanyi)
    {
        ui->textBrowser->setText(showtext);
    }
    else
    {

        ui->textBrowser->setText(curword);

    }
    //动态单词
    Dynamicword(curword);

    //播放
    //存不存在
    QFileInfo fileinfo(curmp3filename);
    if(fileinfo.isFile())
    {
        if(ui->checkBox_xunhuan->isChecked())
        {

            wordclass.Playback_of_audio(curmp3filename);//循环播放

            ui->label_show->setText("循环播放");
            qDebug()<<"循环播放";
        }
        else {

            //danchi_jiange=ui->spinBox_huigu->value();//写错

            wordclass.play_audio_by_jiange(curmp3filename,this->danchi_jiange);//间隔播放,传入标识停止

            ui->label_show->setText("间隔播放");
            qDebug()<<"间隔播放";
        }

    }
    else{
        ui->label_show->setText("未找到单词音频");
        qDebug()<<"未找到单词音频";
    }


    return;

}

void Widget::wild_word_helpStart()
{


    if(current_word_pos<0&&current_word_pos>wordslist.length()-1)
        current_word_pos=0;


    //
    ui->label_show->clear();
    //显示,回顾时不显示
    ui->textBrowser->show();


    if(bo_huigu)
    {
        bo_huigu=false;
        current_word_pos--;

        wild_word_start(wordslist[current_word_pos]);
        return;
    }

    //先停止播放
    //wordclass.stop_audio();//不能在此停止,bool = true

    //显示label
    this->show_currentpos();

    //单词列表同时变化,scroll 滚动
    QModelIndex model= wordlist_model.index(current_word_pos);
    //qDebug()<<model;
    ui->listView->scrollTo(model);


    //单词回顾功能
    bool ishuigu=danchi_huigu();
    if(ishuigu==false)
        //同步播放下一个单词
        wild_word_start(wordslist[current_word_pos]);
    else
    {
        bo_huigu=true;

    }

}



void Widget::show_currentpos()
{

    int sum=wordlist_model.rowCount();
    ui->label->setText(QString("当前位置:%1 (%2) 总单词数:%3").arg(current_word_pos).arg(wordslist[current_word_pos]).arg(sum));

}

bool Widget::savewords_data()
{
    //数据整理
    QString newstr;
    for (int i=0;i<wordslist.count();i++)
    {
        newstr.insert(newstr.length(), wordslist[i].toUtf8()+"\n");

    }

    QFile file(word_data_finame);
    //防止重复添加,能显示
    QString str;
    if(file.open(QIODevice::ReadOnly))
    {
        str=file.readAll();

        file.close();
    }

    else
    {
        ui->label_show->setText("打开word_data_finame文件失败");
        qDebug()<<QString("打开文件失败%1").arg(word_data_finame);
        return false;
    }

    if(str.contains(newstr))
    {
        ui->label_show->setText("已存在相同词组!未添加到历史数据,不再重复保存");
        qDebug()<<"已存在单词组,不要重复添加";
        return false;
    }

    //添加
    if(file.open(QIODevice::Append))
    {
       file.write(QDateTime::currentDateTime().toString("yyyy-MM-dd hh-mm-ss ddd").toUtf8()+"\n");//记录时间

       file.write(newstr.toUtf8());
        file.close();
    }

    return true;

}

void Widget::savewords_data_fengci()
{
    //  \  /  :  *  ?  "  <  >  |    一共9个特殊字符

    QString datatime=QDateTime::currentDateTime().toString("yyyy年MM月dd日hh时mm分ss秒ddd");//文件名不能包含 : //文件名不能包含 ://文件名不能包含 ://文件名不能包含 :
    QString filename=word_data_path+QString("/%1.txt").arg(datatime);
    QFile file(filename);


    //文件名不能包含 :
    if(file.open(QIODevice::ReadWrite))
    {
        QString newstr;
        for (int i=0;i<wordslist.count();i++)
        {
            newstr=newstr.insert(newstr.length(), wordslist[i].toUtf8()+"\n");

        }

        file.write(newstr.toUtf8());


        file.close();
    }
    else{
        qDebug()<<"savewords_data_fengci 错误";
    }


    //保存历史记录

    QFile filelishi(lishi_jilu_filename);

    if(filelishi.open(QIODevice::Append))
    {
        filelishi.write(QString("日期=%1路径=|%2|").arg(datatime).arg(filename).toUtf8()+"\n");

        filelishi.close();
    }

    //更新历史记录
    this->genxing_lishishuju();


}

void Widget::getwords_data(QString word_filename)
{
    if(word_filename.isEmpty())
        return;
    QFileInfo info(word_filename);
    if(info.isFile()==0)
        return;

    QStringList list =wordclass.getwords_bytxt(word_filename);

    //空文件
    if(list.isEmpty())
    {
        ui->label_show->setText("文件为空");
        //qDebug()<<list;
        return;
    }

    this->wordslist=list;
    //添加到单词列表
    wordlist_model.setStringList(wordslist);
    //视图绑定模型
    ui->listView->setModel(&wordlist_model);

    //不需要保存,追加

}

void Widget::helpdaoru()
{
    //先更改wordlist

    //空文件
    if(wordslist.isEmpty())
    {
        ui->label_show->setText("单词列表为空");
        return;
    }
    //添加到列表
    wordlist_model.setStringList(wordslist);
    //视图绑定模型
    ui->listView->setModel(&wordlist_model);
    ui->listView->update();


//先更改wordslist 再以他为数据 进行保存

    //保存数据,追加
    bool buchongfu=this->savewords_data();

    if(buchongfu)//不存在同样数据,再添加版本
      {
        //保存数据,分次
        savewords_data_fengci();//更新历史数据

        ui->label_show->setText("导入数据成功");

        wordclass.getword_mp3_list(wordslist,word_mp3_path);
        ui->label_show->setText("正在批量导入音频数据...请不要操作!");

      }
}


bool Widget::danchi_huigu()
{

    if(ui->checkBox_huigu->isChecked()==false)
        return 0;

    //判断是否到达位置
    //dnahci_huiguschishu 不为0

    //不能为0 int i=current_word_pos-danchi_huigu_chishu=-8  huigulist.append(wordslist[i]);
    //不能为0 int i=current_word_pos-danchi_huigu_chishu=-8  huigulist.append(wordslist[i]);
    //不能为0 int i=current_word_pos-danchi_huigu_chishu=-8  huigulist.append(wordslist[i]);
    //不能为0 danchi_huigu_chishu为除数
    if(danchi_huigu_chishu==0||current_word_pos==0)//不能为0 int i=current_word_pos-danchi_huigu_chishu=-8  huigulist.append(wordslist[i]);
        return 0;

    if(current_word_pos%danchi_huigu_chishu!=0)//10%5=0
        return 0;



    //清除上次单词
    huigulist.clear();
    huigu_fanyi_list.clear();

    ui->label_show->setText("多多回顾,记忆翻倍!");


    //需的回顾的单词10~5
    for(int i=current_word_pos-danchi_huigu_chishu;i<=current_word_pos-1;i++)
    {
        QString word= wordslist[i];

        //获取翻译
        //注意::::单词不能包含特殊字符

        word.remove("\n");
         word.remove("\n\r");
          word.remove("\r\n");
           word.remove("\r");


        huigulist.append(word);

        huigu_fanyi_list.append( wordclass.getword_trans(word));
    }

    //添加到模块
    huigu_model.setStringList(huigulist);
    huigu_fanyi_model.setStringList(huigu_fanyi_list);

    //绑定窗口
    ui->listView_huigu->setModel(&huigu_model);
    ui->listView_huigu_shiyi->setModel(&huigu_fanyi_model);
    ui->listView_huigu->update();
    ui->listView_huigu_shiyi->update();

    //翻译窗口先不显示
    ui->listView_huigu_shiyi->hide();
    //单词窗口不显示
    ui->textBrowser->hide();


    //跳转当前页
    //ui->tabWidget_2->setCurrentIndex(3);
    ui->tabWidget_2->setCurrentWidget(ui->tab_8);
    ui->tabWidget_3->setCurrentWidget(ui->tab_13);
    //
    wordclass.stop_audio();

    return true;

}

void Widget::genxing_lishishuju()
{
    //先清除
    lishishuju_list.clear();

    QFile file(lishi_jilu_filename);
    if(file.open(QIODevice::ReadWrite))
    {
        while (file.atEnd()==false)
        {
            QString str=file.readLine();
            lishishuju_list.append( str );

        }

        file.close();
    }

    //更新页面
    lishishuju_list_medel.setStringList(lishishuju_list);
    ui->listView_lishi_shuju->setModel(&lishishuju_list_medel);
    ui->listView_lishi_shuju->update();

}

void Widget::deletelishishuju(QModelIndex index)
{
    QString deletestr=index.data().toString();
    //qDebug()<<deletestr;

    //删除历史记录
    QFile file(lishi_jilu_filename);
    QString yuanshuju;
    if(file.open(QIODevice::ReadWrite))
    {
        yuanshuju=file.readAll();
        file.close();
    }
    else
    {
        return;
    }

    yuanshuju= yuanshuju.replace(deletestr,"");

    qDebug()<<"yuanshuju"<<yuanshuju;

    if(file.open(QIODevice::WriteOnly))
    {
        file.write(yuanshuju.toUtf8());


        file.close();
    }


    //更新数据
    this->genxing_lishishuju();


    //删除源文件
   //QString filename=lishishuju_help(deletestr);//此函数不好,已更改
    QString filename=getlishifilename(index);

    bool isok=DeleteFileOrFolder(filename);
    qDebug()<<"删除源文件"<<isok;
    ui->label_show->setText(QString("删除源文件:%1").arg(isok));

}

QString Widget::lishishuju_help(QString str)
{
    //日期:2022年04月08日 20-22-03 周五 路径:D:/QTproject/build-Wild_words-Desktop_Qt_5_9_9_MinGW_32bit-Debug/Dada/word_dada/2022年04月08日 20-22-03 周五.txt\n
    //"日期:2022年04月10日-17-31-38-周日 路径:/storage/emulated/0/wild_word狂野单词/Data/word_dada/2022年04月10日-17-31-38-"... (108)
    //android 无D:/ list[3] 超出范围 ， 这样写不好
    QStringList list=str.split(QRegExp("[:]"));
    QString filename;//=list[2]+":"+list[3];
    //多了空格也不能删除//多了空格也不能删除//多了空格也不能删除//多了空格也不能删除//多了空格也不能删除//多了空格也不能删除//多了空格也不能删除//多了空格也不能删除
    //有/n//有/n//有/n//有/n//有/n//有/n//有/n也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除
    //"D:/QTproject/build-Wild_words-Desktop_Qt_5_9_9_MinGW_32bit-Debug/Dada/word_dada/2022年04月08日-21-03-14-周五.txt "//多了空格也不能删除
    //"D:/QTproject/build-Wild_words-Desktop_Qt_5_9_9_MinGW_32bit-Debug/2022年04月08日-21-25-50-周五.txt\n"//有/n也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除也不能删除
    //filename="D:/QTproject/build-Wild_words-Desktop_Qt_5_9_9_MinGW_32bit-Debug/Dada/word_dada/2022年04月08日-21-03-14-周五.txt";

    //解决空格,\n
    //QString strfilename= list[3].split(QRegExp("[/]")).last();
    QString strfilename= list[list.count()-1].split(QRegExp("[/]")).last();
    strfilename=strfilename.remove(" ");
    filename=word_data_path+"/"+strfilename;
    filename=filename.remove("\n");
    filename=filename.remove("\r");
    filename=filename.remove("\n\r");
    filename=filename.remove("\r\n");
    //qDebug()<<filename;

    return filename;
}

void Widget::editlishishuju(QModelIndex index)
{
    QString deletestr=index.data().toString();
    QString filename=lishishuju_help(deletestr);

    QFile file(filename);
    if(file.open(QIODevice::ReadOnly))
    {
        QString  str=file.readAll();
        //ui->tabWidget->setCurrentIndex(3);
        ui->tabWidget->setCurrentWidget(ui->tab_10);
        ui->textEdit->setText(str);

        file.close();
    }

    return;

}

void Widget::daorulishishuju(QModelIndex index)
{
    QString deletestr=index.data().toString();
    QString filename=lishishuju_help(deletestr);
    //导入到单词表
    getwords_data(filename);


}

bool Widget::DeleteFileOrFolder( QString strPath)
{
    strPath=strPath.replace("\\","/");

    if (strPath.isEmpty() || !QDir().exists(strPath))//是否传入了空的路径||路径是否存在
            return false;

        QFileInfo FileInfo(strPath);

        if (FileInfo.isFile())//如果是文件
            QFile::remove(strPath);
        else if (FileInfo.isDir())//如果是文件夹
        {
            QDir qDir(strPath);
            qDir.removeRecursively();
        }
        return true;

}

void Widget::getwordpic(bool newpic)
{

    if(current_word_pos>=0&&current_word_pos<=wordslist.count())
    {
        picturefilename=picepath+"/"+wordslist[current_word_pos]+".jpg";

        if(newpic==true)
            wordclass.getword_picture(wordslist[current_word_pos],picturefilename);
        else
        {

                //检查是否已经存在
            QFileInfo dir(picturefilename);
            if(dir.isFile()==false)
                wordclass.getword_picture(wordslist[current_word_pos],picturefilename);
            else
                this->showwordpic(true);

        }
    }
    else
    {
        ui->label_show->setText("重新导入单词后请重新选择单词");
    }



}

void Widget::showwordpic(bool success)
{
    if(success==0)
    {
        ui->label_show->setText("获取图片失败");
        //防止快速切换
        ui->pushButton_change_pic1->setEnabled(true);
        return;
    }

    ui->label_pic2->setPixmap(QPixmap::fromImage(beforeimg));

    QFileInfo info(picturefilename);//有可能无网络,
    if(info.isFile()==0)
        return;

    QImage img(picturefilename);

    img=img.scaled(ui->label_pic1->width(),ui->label_pic1->height());
    ui->label_pic1->setPixmap(QPixmap::fromImage(img));
    ui->label_pic1->update();
    ui->label_pic2->update();
    this->update();

    beforeimg=img;

    //防止快速切换
    ui->pushButton_change_pic1->setEnabled(true);
}



//bool Widget::requestPermission(QString name)
//{
//    QtAndroid::PermissionResult r = QtAndroid::checkPermission(name);
//    if(r == QtAndroid::PermissionResult::Denied) {
//        QtAndroid::requestPermissionsSync( QStringList() << name );
//        r = QtAndroid::checkPermission(name);
//        if(r == QtAndroid::PermissionResult::Denied) {
//             return false;
//        }
//   }
//    return true;
//}

void Widget::danchi_cheshi()
{
    qDebug("void Widget::danchi_cheshi()");

    if(huigulist.isEmpty())
    {
        ui->label_show->setText("回顾列表为空");
        return;
    }


    QString word=this->huigulist[cheshi_currpos];
    if(word.isEmpty())
        return;

    //播放音频
    QString filename=word_mp3_path+"/"+word+".mp3";
    QFileInfo info(filename);
    if(info.isFile())
        wordclass.play_audio(filename);
    else {

    }

    ui->tabWidget_3->setCurrentWidget(ui->tab_14);

    QString word_trans=huigu_fanyi_list[cheshi_currpos];
    QStringList list;
//    list=wordclass.trans_entochi(word,word.length()/2,3);

    int leftlen=word.length()*(ui->doubleSpinBox_cheshi->value());

    if(leftlen==0)
        leftlen=0;
    QString str=wordclass.entochi(word,leftlen);
    list=str.split(QRegExp("[|]"));

    if(list.length()<3)
    {
        for(int i=0;i<=3-list.length();i++)
            list.append("暂无");
    }
    for(int i=0;i<3;i++)
        if(list[i]=="")
            list[i]="暂无";

    std::random_shuffle(list.begin(), list.end());

    QStringList chilist;
    chilist.append(word_trans);
    chilist.append(list[0]);
    chilist.append(list[1]);
    chilist.append(list[2]);

    //随机排列
    std::random_shuffle(chilist.begin(), chilist.end());

    ui->label_danchi_cheshi->setText(word);

    ui->radioButton->setText(chilist[0]);
    ui->radioButton_2->setText(chilist[1]);
    ui->radioButton_3->setText(chilist[2]);
    ui->radioButton_4->setText(chilist[3]);

}

void Widget::cheshi_showwordpic(QLabel *lab, QString filename)
{


    QFileInfo info(filename);
    if(info.isFile()==0)
    {
        wordclass.getword_picture(huigulist[cheshi_currpos],filename);
        return;
    }
    QImage img(filename);

    img=img.scaled(lab->width(),ui->textBrowser_cheshi->height());
    lab->setPixmap(QPixmap::fromImage(img));
    lab->update();

    this->update();

}

void Widget::radioButton_click(int)
{
    if(huigulist.isEmpty())
    {
        ui->label_show->setText("回顾列表为空");
        return;
    }

    QString str=buttgroup.checkedButton()->text();
    QString word=huigulist[cheshi_currpos];
    QString word_trans=huigu_fanyi_list[cheshi_currpos];

    if(str==word_trans)
    {
        ui->textBrowser_cheshi->setText(tr("答对了!\n%1\n%2").arg(word).arg(word_trans));
    }
    else
    {
        //ui->textBrowser_cheshi->setText(tr("答错了!\n%1\n%2").arg(word).arg(word_trans));
        ui->textBrowser_cheshi->setText(tr("答错了!\n%1").arg(word));
        //从新播放
        this->danchi_cheshi();
        return;
    }

    //图片
    QString filename=picepath+"/"+word+".jpg";
    cheshi_showwordpic(ui->label_cheshi_pic,filename);
    //动态单词
    Dynamicword(word);

    cheshi_currpos++;
    if(cheshi_currpos>=huigulist.length())
    {

        ui->label_show->setText("恭喜你完成单词测试!");
        cheshi_currpos=0;

    }

    this->danchi_cheshi();
}

void Widget::Dynamicword(QString word)
{
    if(ui->checkBox_dongtai_close->isChecked())
        return;


    QStringList list=wordclass.syllabification(word);
    //音节划分label
    //缩减板块
    QString str1,str2;
    int num=list.length();
    if(num==1)
    {
        ui->label_yingjie_huafen->setText(list[0]);
        //动态单词
        dynamicword(word);
        return;
    }
    else if(num%2==0)
    {

        for(int i=0;i<list.length();i++)
            {
                if(i+1>num/2)
                    str2.append(list[i]);
                else
                    str1.append(list[i]);


            }

    }
    else
    {//奇数
        for(int i=0;i<list.length();i++)
            {
                if(i>num/2)
                    str2.append(list[i]);
                else
                    str1.append(list[i]);


            }

    }
    QString text;
    text=str1+"-"+str2+" = ";

    //动态单词
    dynamicword(word+"="+str1+"\\"+str2);


    for(int i=0;i<list.length();i++)
        text.append(list[i]+" ");

    ui->label_yingjie_huafen->setText(text);



}

void Widget::dynamicword(QString word)
{


    if(word.isEmpty())
    {
        if(dongtai_list.isEmpty())
            return;

        QString str;
        for(int i=0;i<=dongtai_currpos;i++)
            str.append(dongtai_list[i]);

//        QStringList listnum;listnum.append("20");listnum.append("21");listnum.append("22");listnum.append("23");listnum.append("24");listnum.append("25");
//        std::random_shuffle(listnum.begin(),listnum.end());
//        QString size=listnum[0];
//        QFont font;font.setPointSize(size.toInt());
//        ui->label_dongtai_word->setFont(font);

        ui->label_dongtai_word->setText(str);
        ui->label_dongtai_word->update();

        dongtai_currpos++;
        if(dongtai_currpos>dongtai_list.length()-1)
        {
            dongtai_currpos=0;
            dongtai_timer_jiange.start(ui->spinBox_dongtai_jiange->value());
            return;
        }

        dongtai_timer.start(ui->spinBox_dongtai_msec->value());
    }
    else
    {
        dongtai_list.clear();
        dongtai_currpos=0;
        //更新列表
        QByteArray arr;
        arr.append(word);
        char*cha=arr.data();
        for(int i=0;i<word.length();i++)
        {
            QString str(cha[i]);
            dongtai_list.append(str);
        }
        dynamicword();

    }



}

void Widget::xunhuan_bofan_liepiao()
{
    if(ui->checkBox_stop_xunhuan_currlist->isChecked())
    {
        ui->label_show->setText("已停止循环播放列表");
        wordclass.stop_audio();
        if(current_word_pos==-1)
            current_word_pos=0;
        return;
    }

    if(current_word_pos+1>wordslist.count()-1)
    {
        current_word_pos=-1;
    }

    //定时点击下一个
    on_pushButton_next_clicked();

    timer_xunhuan_bofan_liebiao.start(ui->spinBox_bofang_liebao->value());

}



int Widget::setdatapxath()
{


    QFile file(QDir::currentPath()+"/path.txt");
    if(file.open(QIODevice::ReadWrite))
    {
        QString str=file.readLine();


        if(str.isEmpty())
        {
            //华为不会自动开启 存储权限
//            requestPermission("android.permission.WRITE_EXTERNAL_STORAGE");
//            requestPermission("android.permission.READ_EXTERNAL_STORAGE");
//            requestPermission("android.permission.WRITE_EXTERNAL_STORAGE");

            //
            QMessageBox customMsgBox;
            QPushButton*ok = customMsgBox.addButton("数据放在内部",QMessageBox::ActionRole);
            QPushButton*no = customMsgBox.addButton("数据放在外部",QMessageBox::ActionRole);

            customMsgBox.setText("内部:数据跟随app删除,用户不可访问修改\n外部:数据不跟随app删除,用户可访问修改");//设置窗口文字提示
            customMsgBox.setWindowTitle("选择数据存放位置");//设置窗口标题

            customMsgBox.exec();//显示窗口

            //定义对话框响应事件
            if(customMsgBox.clickedButton()==ok){

                file.write("数据放在内部");
                file.close();
                qDebug()<<"数据放在内部";
                return 1;
            }

            if(customMsgBox.clickedButton()==no){
                file.write("数据放在外部");

                file.close();
                qDebug()<<"数据放在外部";
                return 2;
            }


        }
        else
        {
                qDebug()<<"已选择过data路径";
                if(str.contains("数据放在内部"))
                {
                    file.close();
                    qDebug()<<"数据放在内部";
                    return 1;

                }
                else
                {
                    file.close();
                    qDebug()<<"数据放在外部";
                    return 2;
                }
        }

        file.close();
    }
    else
    {
        qDebug()<<"datapath:打开文件失败";
    }

    return 1;

}

void Widget::showbeijingtu()
{

    QImage yuan(beijintuname);
    yuan=yuan.scaled(this->size());

    QString filename=picepath+"/背景图.jpg";
    yuan.save(filename);

    this->setStyleSheet(QString("background-color: rgb(255, 255, 225, 80);background-image:url(%1)").arg(filename));
    this->update();

}


/*
void Widget::on_openfile_clicked()
{
    //获取单词
    QString str=QFileDialog::getOpenFileName(this,"选择excel文件","","*xlsx");
    if(str.isEmpty())
        return;

    QStringList list=wordclass.getwords_ForExcel(str);
    if(list.isEmpty())
        return;

    this->wordslist=list;
    this->helpdaoru();



}
*/



void Widget::on_pushButton_next_clicked()
{

    int pos=current_word_pos+1;
    if(pos>wordslist.count()-1)
        return;

    //跳转窗口
    //ui->tabWidget_2->setCurrentIndex(0)不好
    ui->tabWidget_2->setCurrentWidget(ui->tab_3);

    current_word_pos=pos;//同步当前单词位置

    //
    this->wild_word_helpStart();


    //防止快速点击
    ui->pushButton_next->setEnabled(false);
    timer_dianji.start(1000);

}

void Widget::on_pushButton_wordslist_clicked()
{
    if(ui->dockWidget->isHidden()==false)
    {
        ui->dockWidget->setFloating(false);
        ui->dockWidget->setVisible(false);

        return;
    }
    //
    //ui->tabWidget_2->setCurrentIndex(2);
    ui->tabWidget_2->setCurrentWidget(ui->tab_6);

    //显示
    ui->dockWidget->setFloating(false);
    ui->dockWidget->setVisible(true);
}

void Widget::on_listView_clicked(const QModelIndex &index)
{
//2 QVariant(QString, "3")
//    qDebug()<<ui->listView->currentIndex().row()<<
//    ui->listView->currentIndex().data();

    //跳转窗口,不用
    //ui->tabWidget_2->setCurrentIndex(0);

    //同步
    current_word_pos=ui->listView->currentIndex().row();

   //
    this->wild_word_helpStart();

    ui->listView->setEnabled(false);
    timer_dianji.start(1000);

}

void Widget::on_pushButton_clicked()
{

    //播放器的停止

    //player->state()会返回当前播放器的状态，:PlayingState(值为1)为播放中状态，对应的还有StoppedState（值为0）空闲状态，PausedState（值为2）播放暂停状态。
    int state=wordclass.get_aplay_state();

    //qDebug()<<state;
    if(state==0||state==2)
    {
         //wordclass.stop_audio();//不能在此停止 bool =ture 了 //间隔模式 无效
        //    //间隔模式
        //stop_jiangetime=true;

        if(ui->checkBox_xunhuan->isChecked())
            wordclass.start_audio();//循环模式

        else {

            wordclass.play_audio_by_jiange(curmp3filename,danchi_jiange);//间隔模式

        }
    }
    else {

        wordclass.stop_audio();


        }

    //防止快速点击
    ui->pushButton->setEnabled(false);
    timer_dianji.start(1000);

}

void Widget::on_pushButton_last_clicked()
{


    int pos=current_word_pos-1;

    if(pos<0)
        return;

    //跳转窗口
    //ui->tabWidget_2->setCurrentIndex(0);
    ui->tabWidget_2->setCurrentWidget(ui->tab_3);

    current_word_pos=pos;//同步当前单词位置

    //
    this->wild_word_helpStart();

    //防止快速点击
    ui->pushButton_last->setEnabled(false);
    timer_dianji.start(1000);
}

void Widget::on_openfile_bytxt_clicked()//这个函数可合并
{
    QMessageBox customMsgBox;
    //添加自定义按钮
    QPushButton*ok = customMsgBox.addButton("确认",QMessageBox::ActionRole);

    QPushButton*close = customMsgBox.addButton(QMessageBox::Cancel);
    customMsgBox.setText("1.确认app开启了存储权限\n2.请将之前导出的app数据文件夹[wild_word狂野单词导出数据]放在手机根目录下\n3.文件夹名[wild_word狂野单词导出数据]不要修改!");//设置窗口文字提示
    customMsgBox.setWindowTitle("确认操作");//设置窗口标题

    customMsgBox.exec();//显示窗口
    //定义对话框响应事件
    if(customMsgBox.clickedButton()==ok)
    {
        bool bo=daoru_appdata();
        if(bo==0)
            ui->label_show->setText("导入失败!!!!!!!!");
        else
        {

            ui->label_show->setText("导入数据成功!");
            getwords_data(this->word_data_finame);//
            genxing_lishishuju();//记得更新不然闪退
        }
    }

    if(customMsgBox.clickedButton()==close){
         customMsgBox.close();
    }

   return;

}

//void Widget::on_danchibofan_jiange_valueChanged(double arg1)//注意,部件提升后,槽函数并未改变,造成bug
//{
//    danchi_jiange=arg1;
//    qDebug()<<arg1;
//}

void Widget::on_danchibofan_jiange_valueChanged(int arg1)
{
    danchi_jiange=arg1;
    //qDebug()<<arg1;
}

void Widget::on_spinBox_huigu_valueChanged(int arg1)
{
    danchi_huigu_chishu=arg1;
}

void Widget::on_pushButton_lookshiyi_clicked()
{

    //显示翻译
    if(ui->listView_huigu_shiyi->isHidden() )
       {
        ui->listView_huigu_shiyi->show();
       }

    else {
        ui->listView_huigu_shiyi->hide();
    }

}


void Widget::on_listView_huigu_clicked(const QModelIndex &index)
{
    //回顾单词,点击播放音频

    QString str=huigulist[index.row()];

    wild_word_start(str);

    //动态单词
    Dynamicword(str);

    ui->listView_huigu->setEnabled(false);
    timer_dianji.start(1000);
}


void Widget::on_pushButton_chengchiben_clicked()
{
    //导入生词本

    showcureditfile(shengchi_filename);
}


void Widget::on_listView_lishi_shuju_doubleClicked(const QModelIndex &index)
{
    qDebug()<<"on_listView_lishi_shuju_doubleClicked";

   QMessageBox customMsgBox;
   //添加自定义按钮
   QPushButton*ok = customMsgBox.addButton("删除",QMessageBox::ActionRole);
   QPushButton*no = customMsgBox.addButton("导入,编辑",QMessageBox::ActionRole);
   QPushButton*edit_name = customMsgBox.addButton("更改文件名",QMessageBox::ActionRole);
   QPushButton*close = customMsgBox.addButton(QMessageBox::Cancel);
   customMsgBox.setText("今天也要开心呀!");//设置窗口文字提示
   customMsgBox.setWindowTitle("选择操作");//设置窗口标题

   customMsgBox.exec();//显示窗口
   //定义对话框响应事件
   if(customMsgBox.clickedButton()==ok){
       this->deletelishishuju(index);

   }

   if(customMsgBox.clickedButton()==no){


       showcureditfile(getlishifilename(index));
   }

   if(customMsgBox.clickedButton()==edit_name){

       QString newname=ui->lineEdit_edit_lishi_filename->text(),oldfilename;

        if(newname.isEmpty())
        {
            ui->label_show->setText("文件名为空");
            return;
        }


        oldfilename=getlishifilename(index);
        QFileInfo info(oldfilename);
        newname=info.path()+"/"+newname+".txt";

        //修改文件
        QFile file(oldfilename);
        bool ok=file.rename(newname);
        if(ok==false)
        {
            ui->label_show->setText("重命名失败,是否存在相同文件名?");
            return;
        }

        //修改历史数据列表
        QString str;
        QFile filelishi(lishi_jilu_filename);
        if(filelishi.open(QIODevice::ReadOnly))
        {
            str=filelishi.readAll();

            filelishi.close();
        }
        if(filelishi.open(QIODevice::WriteOnly))
        {
            str=str.replace(oldfilename,newname);
            filelishi.write(str.toUtf8());

            filelishi.close();
        }

        //更新列表
        genxing_lishishuju();

   }

   if(customMsgBox.clickedButton()==close){
        customMsgBox.close();
   }


  return;

}

void Widget::on_listView_huigu_shiyi_doubleClicked(const QModelIndex &index)
{

    qDebug()<<"on_listView_huigu_shiyi_doubleClicked";

    QMessageBox customMsgBox;
    //添加自定义按钮
    QPushButton*ok = customMsgBox.addButton("确定添加生词本",QMessageBox::ActionRole);

    QPushButton*close = customMsgBox.addButton(QMessageBox::Cancel);
    customMsgBox.setText("笑一笑十年少!");//设置窗口文字提示
    customMsgBox.setWindowTitle("选择操作");//设置窗口标题
    //customMsgBox.setIconPixmap(QPixmap("01.png"))//设置图标
    customMsgBox.exec();//显示窗口
    //定义对话框响应事件
    if(customMsgBox.clickedButton()==ok){

        //双击添加生词本
        QString wordstr=huigulist[index.row()];

        QString time=QDateTime::currentDateTime().toString("---yyyy年MM月dd日 ddd---");//分割线

        QString str;
        QFile file(shengchi_filename);
        if(file.open(QIODevice::ReadWrite))
        {
            str=file.readAll();


            //让指针回到开头,在末尾了
            file.seek(0);


            //写入
            file.write(str.toUtf8());
            if(str.contains(time)==false)
                file.write(time.toUtf8()+"\n");

            file.write(wordstr.toUtf8()+"\n");


            file.close();
        }

        ui->label_show->setText("已添加到生词词本");
    }


    if(customMsgBox.clickedButton()==close){
         customMsgBox.close();
    }

   return;



}

void Widget::on_pushButton_dele_jushuh_clicked()
{
    QString text;
    text=ui->textEdit->toPlainText();

    text=text.remove("\r");
    QStringList list;
    list=text.split("\n");


    text.clear();
    for(int i =0;i<list.count();i++)
    {
        if(i%2==0)
        {
            text.append(list[i]+"\n");
        }


    }

    ui->textEdit->clear();
    ui->textEdit->setPlainText(text);

}

void Widget::on_pushButton_dele_oushuh_clicked()
{
    QString text;
    text=ui->textEdit->toPlainText();

    text=text.remove("\r");
    QStringList list;
    list=text.split("\n");


    text.clear();
    for(int i =0;i<list.count();i++)
    {
        if(i%2!=0)
        {
            text.append(list[i]+"\n");
        }


    }

    ui->textEdit->clear();
    ui->textEdit->setPlainText(text);
}

void Widget::on_pushButton_daoru_text_clicked()
{
    //先更新 wordslist
    QString text;
    text=ui->textEdit->toPlainText();

    text=text.remove("\r");
    text=text.replace("\n\n","\n");

    QStringList list;
    list=text.split("\n");

    if(list.isEmpty())
        return;

    wordslist=list;

    this->helpdaoru();

}

void Widget::on_pushButton_textedit_celer_clicked()
{
    ui->textEdit->clear();
}



void Widget::on_pushButton_2_clicked()
{
    //QDesktopServices::openUrl(QUrl::fromLocalFile(word_data_path));//不用

    showcureditfile(word_data_finame);


}

void Widget::on_pushButton_change_pic1_clicked()
{
    ui->pushButton_change_pic1->setEnabled(false);

    this->getwordpic(true);

//    等待回应再可按
//    ui->pushButton_change_pic1->setEnabled(false);
//    timer_dianji.start(1000);

}


//void Widget::closeEvent(QCloseEvent *event)
//{

//    QFileInfo FileInfo(picepath);
//    if(FileInfo.isDir())//如果是文件夹
//    {
//        QDir qDir(picepath);
//        qDebug()<<"删除图片:"<< qDir.removeRecursively();
//    }

//    qApp->quit();
//}


void Widget::on_pushButton_4_clicked()
{
    QMessageBox customMsgBox;
    //添加自定义按钮
    QPushButton*ok = customMsgBox.addButton("图1",QMessageBox::ActionRole);
    QPushButton*no = customMsgBox.addButton("图2",QMessageBox::ActionRole);
    //添加系统按钮
    QPushButton*cancle = customMsgBox.addButton("图3",QMessageBox::ActionRole);
    QPushButton*close = customMsgBox.addButton(QMessageBox::Cancel);
    customMsgBox.setText("今天也要开心呀!");//设置窗口文字提示
    customMsgBox.setWindowTitle("选择背景图");//设置窗口标题
    //customMsgBox.setIconPixmap(QPixmap("01.png"))//设置图标
    customMsgBox.exec();//显示窗口
    //定义对话框响应事件
    if(customMsgBox.clickedButton()==ok){
        beijintuname=":/pic1";

    }

    if(customMsgBox.clickedButton()==no){
        beijintuname=":/pic2";
    }

    if(customMsgBox.clickedButton()==cancle){
         beijintuname=":/pic3";


    }
    if(customMsgBox.clickedButton()==close)
    {
        customMsgBox.hide();
        customMsgBox.close();
    }

    //
    //this->showbeijingtu();
    resizeEvent(nullptr);
}


void Widget::on_pushButton_5_clicked()
{

    QFileInfo FileInfo(picepath);//直接删除路径,本次运行无法使用picpath
    if(FileInfo.isDir())//如果是文件夹
    {
        QDir qDir(picepath);
        bool bo= qDir.removeRecursively();
        qDebug()<<"删除图片:"<<bo;
        ui->label_show->setText(QString("删除图片:%1").arg(bo));
    }

    QDir dir;
    picepath=exepath+"/word_pic";
    if(dir.exists(picepath)==false)
    {
        dir.mkpath(picepath);

    }

}



void Widget::on_pushButton_6_clicked()
{

    QString text;
    text=ui->textEdit->toPlainText();

    text=text.remove("\r");
    text=text.replace("\n\n","\n");

    ui->textEdit->clear();
    ui->pushButton_daoru_text->setEnabled(true);
    ui->pushButton_6->setEnabled(false);

    if(text.isEmpty())
        return;


    QFile file(crueidtfile);
    if(file.open(QIODevice::WriteOnly))
    {
        file.write(text.toUtf8());

        file.close();
        this->getwords_data(crueidtfile);
        ui->label_show->setText("文件已修改!并导入单词表");
    }



}

void Widget::on_pushButton_7_clicked()
{
    //开启权限
    //我后面发现原来华为并没有 默认给 程序存储 权限!!!!!!! 所以方法一才才不行!!!!
    //我后面发现原来华为并没有 默认给 程序存储 权限!!!!!!!
    //我后面发现原来华为并没有 默认给 程序存储 权限!!!!!!!

    QMessageBox customMsgBox;
    //添加自定义按钮
    QPushButton*ok = customMsgBox.addButton("确定导出数据",QMessageBox::ActionRole);

    QPushButton*close = customMsgBox.addButton(QMessageBox::Cancel);
    customMsgBox.setText("笑一笑!");//设置窗口文字提示
    customMsgBox.setWindowTitle("选择操作");//设置窗口标题

    customMsgBox.exec();//显示窗口
    //定义对话框响应事件
    if(customMsgBox.clickedButton()==ok){


        //方法1:( 对华为不行 原来是没有开启权限
        QString path=daochupath;
        QDir dri;

        DeleteFileOrFolder(path);
        dri.mkpath(path);

        bool bo=copyDirectoryFiles(exepath,path,true);
        QString str=tr("导出:%1;%2;%3").arg(path).arg(bo).arg("\n失败请记得开启存储权限");
        qDebug()<<str;
        ui->label_show->setText(str);

    }

    if(customMsgBox.clickedButton()==close){
         customMsgBox.close();
    }




    /*
    //方法2
    //QFileDialog::getExistingDirectory() andriod 下不是绝对路径 "conect......." 不能用 dir()不能识别?

    */
}




//拷贝文件夹：
bool Widget::copyDirectoryFiles(const QString &fromDir, const QString &toDir, bool coverFileIfExist)
{
    if(toDir.isEmpty())
        return false;

    QDir sourceDir(fromDir);
    QDir targetDir(toDir);
    if(!targetDir.exists())
    {    /**< 如果目标目录不存在，则进行创建 */
        if(!targetDir.mkdir(targetDir.absolutePath()))
            return false;
    }

    QFileInfoList fileInfoList = sourceDir.entryInfoList();
    foreach(QFileInfo fileInfo, fileInfoList){
        if(fileInfo.fileName() == "." || fileInfo.fileName() == "..")
            continue;

        if(fileInfo.isDir()){    /**< 当为目录时，递归的进行copy */
            if(!copyDirectoryFiles(fileInfo.filePath(),
                targetDir.filePath(fileInfo.fileName()),
                coverFileIfExist))
                return false;
        }
        else{            /**< 当允许覆盖操作时，将旧文件进行删除操作 */
            if(coverFileIfExist && targetDir.exists(fileInfo.fileName())){
                targetDir.remove(fileInfo.fileName());
            }

            /// 进行文件copy
            if(!QFile::copy(fileInfo.filePath(),
                targetDir.filePath(fileInfo.fileName()))){
                    return false;
            }
        }
    }
    return true;
}

bool Widget::cepyfiletopath(QString filename, QString path)
{
    QDir dri;
    if(!dri.exists(path)||!dri.exists(filename))
        return 0;


    QFileInfo info(filename);
    QFile fileyuan(filename),filecopy(path+"/"+info.fileName());

    if(fileyuan.open(QIODevice::ReadWrite)&&filecopy.open(QIODevice::ReadWrite))
    {

        filecopy.write(fileyuan.readAll());


        fileyuan.close();
        filecopy.close();
    }
    else
    {
        return false;
    }
    return true;
}

void Widget::showcureditfile(QString cur_xiugenfilename)
{
    crueidtfile=cur_xiugenfilename;

    QMessageBox customMsgBox;
    //添加自定义按钮
    QPushButton*no = customMsgBox.addButton("编辑",QMessageBox::ActionRole);
    //添加系统按钮
    QPushButton*cancle = customMsgBox.addButton("导入",QMessageBox::ActionRole);
    QPushButton*close = customMsgBox.addButton(QMessageBox::Cancel);
    customMsgBox.setText("今天也要开心呀!");//设置窗口文字提示
    customMsgBox.setWindowTitle("选择操作");//设置窗口标题

    customMsgBox.exec();//显示窗口
    //定义对话框响应事件

    if(customMsgBox.clickedButton()==no){

        ui->pushButton_daoru_text->setEnabled(false);
        ui->pushButton_6->setEnabled(true);

        QFile file(cur_xiugenfilename);
        if(file.open(QIODevice::ReadOnly))
        {
            QString str=file.readAll();
            ui->textEdit->setText(str);
            //ui->tabWidget->setCurrentIndex(3);
            ui->tabWidget->setCurrentWidget(ui->tab_10);
            file.close();
        }


    }

    if(customMsgBox.clickedButton()==cancle){
         getwords_data(cur_xiugenfilename);
    }
    if(customMsgBox.clickedButton()==close){
         customMsgBox.close();
    }

    return;
}

QString Widget::getlishifilename(QModelIndex index)
{
    QString str= index.data().toString();
    str=str.remove("\n");
    str=str.remove("\r\n");

    if(str.isEmpty())
        return nullptr;//bug来源[1]超出范围

    str=str.split(QRegExp("[|]"))[1];//bug来源[1]超出范围

    return str;

}

bool Widget::daoru_appdata()
{
    QString yuan=daochupath;

    QDir dir(yuan);
    if(dir.exists(yuan)==0)
    {

        return false;
    }

    DeleteFileOrFolder(exepath);
    dir.mkpath(exepath);

    bool bo=copyDirectoryFiles(yuan,exepath,true);
    if(bo==0)
    {
        return false;
    }
    else
    {
        return true;
    }


}



void Widget::on_pushButton_3_clicked()
{
    if(wordslist.isEmpty())
    {
        ui->label_show->setText("单词列表为空");
        return;
    }
    QDesktopServices::openUrl(QUrl( QLatin1String( tr("https://dict.youdao.com/w/%1/#keyfrom=dict2.top").arg(wordslist[current_word_pos]).toUtf8().data() ) ));
}


void Widget::on_pushButton_8_clicked()
{
        QMessageBox customMsgBox;
        //添加自定义按钮
        QPushButton*no = customMsgBox.addButton("CSDN",QMessageBox::ActionRole);
        //添加系统按钮
        QPushButton*cancle = customMsgBox.addButton("B站",QMessageBox::ActionRole);
        QPushButton*close = customMsgBox.addButton(QMessageBox::Cancel);
        customMsgBox.setText("今天也要开心呀!");//设置窗口文字提示
        customMsgBox.setWindowTitle("选择");//设置窗口标题

        customMsgBox.exec();//显示窗口
        //定义对话框响应事件

        if(customMsgBox.clickedButton()==no)
        {

            QDesktopServices::openUrl(QUrl(QLatin1String("https://blog.csdn.net/qq_62595450?spm=1000.2115.3001.5343")));

        }

        if(customMsgBox.clickedButton()==cancle){
             QDesktopServices::openUrl(QUrl(QLatin1String("https://space.bilibili.com/500047662?spm_id_from=333.1007.0.0")));
        }

        if(customMsgBox.clickedButton()==close){
             customMsgBox.close();
        }


}


void Widget::on_spinBox_valueChanged(int arg1)
{
    ui->tabWidget->setStyleSheet(tr("background-color: rgba(255, 255, 255, %1);").arg(arg1));
    ui->tabWidget->update();
    this->update();
}



void Widget::on_pushButton_9_clicked()
{
    //查字典

    //可能文本框已经hide
    ui->textBrowser->show();

    QString word=ui->lineEdit_chidian_inp->text();
    word=word.remove("\n");

    QString fanyi;
    bool mohu=ui->checkBox_mohu->isChecked();

    if(wordclass.IsEnglish(word))
        fanyi=wordclass.getword_trans_en_chi(word,mohu);
    else
        fanyi=wordclass.getword_trans_chi_en(word,mohu);



    ui->textBrowser->setText(fanyi);


}


void Widget::on_pushButton_10_clicked()
{
    QString str=ui->lineEdit_chidian_inp->text();
    if(str.isEmpty())
        return;

    QString text;
    QFile file(chidianbenfilename);
    if(file.open(QIODevice::ReadOnly))
    {
        text=file.readAll();


        file.close();
    }

    if(text.contains(str))
    {
        QMessageBox box(this);
        box.setText("已经存在相同单词,是否添加?");
        box.setWindowTitle("选择操作");
        QPushButton *ok,*cancel;
        ok=box.addButton("添加",QMessageBox::ActionRole);
        cancel=box.addButton("取消",QMessageBox::ActionRole);

        box.exec();
        if(box.clickedButton()==cancel)
            return;

    }

    if(file.open(QIODevice::Append))
    {
        QString time=QDateTime::currentDateTime().toString("---yyyy年MM月dd日 ddd---");//分割线
        if(text.contains(time)==false)
            file.write(time.toUtf8()+"\n");

        file.write(QString(str+"\n").toUtf8());
        file.close();
        ui->label_show->setText("已保存到词典本");
    }
}


void Widget::on_pushButton_11_clicked()
{
    showcureditfile(chidianbenfilename);
}


void Widget::on_pushButton_12_clicked()
{
    ui->textBrowser->show();
    if(curshow_wordfanyi)
    {
        ui->textBrowser->setText(curword);
        curshow_wordfanyi=false;
    }
    else
    {

        ui->textBrowser->setText(showtext);
        curshow_wordfanyi=true;
    }
}


void Widget::on_pushButton_13_clicked()
{
    ui->textBrowser->show();

    if(curshow_wordfanyi)
    {
        ui->textBrowser->setText(curfanyi);
        curshow_wordfanyi=false;
    }
    else
    {

        ui->textBrowser->setText(showtext);
        curshow_wordfanyi=true;
    }
}


void Widget::on_tabWidget_2_currentChanged(int index)
{
    //单词回顾,隐藏文本框
    if(index==3)
        ui->textBrowser->hide();

    ui->tabWidget_2->resize(ui->tabWidget_2->width(),(this->height()/3)*2);
}


void Widget::on_pushButton_14_clicked()
{
    //可能文本框已经hide
    ui->textBrowser->show();

    curword=curword.remove("\n");

    QString fanyi;

    fanyi=wordclass.getword_trans_en_chi(curword,true);


    ui->textBrowser->setText(fanyi);
}


void Widget::on_pushButton_15_clicked()
{
    qDebug("on_pushButton_15_clicked");
    if(huigulist.isEmpty())
    {
        ui->label_show->setText("回顾列表为空");
        return;
    }

    wordclass.stop_audio();

    cheshi_currpos=0;

    this->danchi_cheshi();

}



void Widget::on_lineEdit_chidian_inp_textChanged(const QString &arg1)
{
    ui->textBrowser->show();

    QString word=arg1;
    word=word.remove("\n");

    QString fanyi;
    bool mohu=ui->checkBox_mohu->isChecked();

    if(wordclass.IsEnglish(word))
        fanyi=wordclass.getword_trans_en_chi(word,mohu);
    else
        fanyi=wordclass.getword_trans_chi_en(word,mohu);



    ui->textBrowser->setText(fanyi);

    //动态单词
    Dynamicword(word);
}



void Widget::on_spinBox_dongtai_msec_valueChanged(int arg1)
{
    //dongtai_word_sec=arg1;
}


void Widget::on_pushButton_16_clicked()
{
    current_word_pos=-1;
    xunhuan_bofan_liepiao();

}


void Widget::on_checkBox_stop_xunhuan_currlist_stateChanged(int arg1)
{
    if(ui->checkBox_stop_xunhuan_currlist->isChecked()==false)
        xunhuan_bofan_liepiao();
    else
    {
        wordclass.stop_audio();
        timer_xunhuan_bofan_liebiao.stop();

    }
}

