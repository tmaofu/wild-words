#include "words.h"
#include<QCoreApplication>
#include<QMessageBox>
words::words(QObject *parent) : QObject(parent)
{

    qDebug()<<"配置:"<<managerlist.supportedSchemes();

    qDebug()<<"需要:" << QSslSocket::sslLibraryBuildVersionString();

        qDebug()<<"返回包含当前项目的可执行文件的路径"<<QCoreApplication::applicationDirPath();
//    Returns the directory that contains the application executable.
//    返回包含当前项目的可执行文件的路径;
        qDebug()<<"返回可执行文件的路径"<<QCoreApplication::applicationFilePath();



    //3.链接请求结束(保存mp3)
    connect(&manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(ReplyFinished(QNetworkReply*)));

    //
    connect(&managerlist,SIGNAL(finished(QNetworkReply*)),this,SLOT(ReplyFinishedlist(QNetworkReply*)));

    //QNetworkReply* 是不是 managerlist 的指针 , delete 之后 QNetworkReply *未=null andorid 报错? 请求 managerlist ,返回的QNetworkReply
    connect(&timer_mp3list,&QTimer::timeout,[=]()
    {
        timer_mp3list.stop();
        biaoshi++;

        this->getmpslist();

    }
    );

    //图片
    connect(&manager_pic,&QNetworkAccessManager::finished,[=](QNetworkReply*reply){

        savewordpic(reply);


    });
    connect(&manager_pic_http,&QNetworkAccessManager::finished,[=](QNetworkReply*reply){

        getwordpic_http(reply);


    });

    //播放间隔
    connect(&timer_jiange,&QTimer::timeout,this,&words::slot_jiange);



}


/*
QStringList words::getwords_ForExcel(QString excelfilename)
{
    QStringList strlist;
//    //
//    QAxObject excelobject("Excel.Application");
//    //可见
//    excelobject.setProperty("Visisble",true);
//    //打开文档
//    QAxObject *workbooks=excelobject.querySubObject("WorkBooks");
//    workbooks->dynamicCall("Open(const QString&)",this->excfilename);
//    //获取第一个表
//    QAxObject*worksheets=workbooks->querySubObject("Worksheets(int)",1);
//    //获取cell的值

    QAxObject excel("Excel.Application");
    excel.setProperty("Visible",false);
    QAxObject *workbooks = excel.querySubObject("WorkBooks");
    workbooks->dynamicCall("Open (const QString&)",excelfilename);                             //路径在这里！！！！
    QAxObject *workbook = excel.querySubObject("ActiveWorkBook");//获取活动工作簿
    QAxObject *worksheets = workbook->querySubObject("WorkSheets");//获取所有的工作表,如图
    int intCount = worksheets->property("Count").toInt();           //获取了表的个数
    //qDebug()<<intCount;
    QAxObject *worksheet = workbook->querySubObject("WorkSheets(int)",1);//获取第一个工作表



  //  QAxObject *range = worksheet->querySubObject("Cells(int,int)",1,1); //获取cell的值
    QAxObject *used_range = worksheet->querySubObject("UsedRange");     //获得利用的范围
    QAxObject *rows  = used_range->querySubObject("Rows");
    QAxObject *columns = used_range->querySubObject("Columns");
    int row_start = used_range->property("Row").toInt();          //获得开始行
  //  qDebug()<<row_start;              //已经验证准确
    int column_start  = used_range->property("Column").toInt();     //获得开始列
    int row_count = rows->property("Count").toInt();
   // qDebug()<<row_count;              //已经验证准确
    int column_count = columns->property("Count").toInt();
  //  QString strVal = range->dynamicCall("Value2()").toString();
    for(int i = row_start;i<=row_count;i++){
        for(int j = column_start;j<=column_count;j++){
            QAxObject *cell = worksheet->querySubObject("Cells(int,int)",i,j);
           // QString cell_value = cell->property("Value").toString();
           QString str= cell->dynamicCall("Value2()").toString();             //只有这句才好使，能够确保能够读取信息，各个信息存于后面那个变量中

           strlist.append(str);
           //qDebug()<<i<<j<<str;
        }
        //qDebug()<<endl;
    }
    excel.dynamicCall("Quit(void)");               //加上这行实现了对文件的释放

    //ui->label->setText(strVal);

    return strlist;
}
*/

QStringList words::getwords_bytxt(QString txtfilename)
{

    QStringList wordlist;
    QFile file(txtfilename);
    if(file.open(QIODevice::ReadWrite))
    {
        QString strall=file.readAll();
        if(strall.isEmpty())
        {
            qDebug()<<"getwords_bytxt:文件为空";
            return wordlist;
        }

        //让指针回到开头,否则while 无效,在末尾了
        file.seek(0);

        while (file.atEnd()==false)
        {
            QString str=file.readLine();


            //获取翻译
            //注意::::单词不能包含特殊字符
            str.remove("\n\r");
            str.remove("\r");
            str.remove("\n");
            str.remove("\r\n");
            wordlist.append(str);
        }


        file.close();
    }
    else
    {
        qDebug()<<" QStringList words::getwords_bytxt(QString txtfilename) 打开文件失败";
    }
    return wordlist;


}

void words::getword_mp3(QString word_str,QString outfilename)
{
    wordmp3_filename=outfilename;

    word_str=word_str.remove("\n");
    word_str=word_str.remove("\n\r");
    word_str=word_str.remove("\r\n");
    word_str=word_str.remove("\r");

    QString url = QString("http://dict.youdao.com/dictvoice?type=1&audio=%1").arg(word_str);     //获取url
//    qDebug()<<url;
    //1. 创建一个请求
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    //2.管理器
    //3.链接请求结束(保存mp3)
    //4.发送
    manager.get(request);

    //这里直接返回,不会等待;

}

void words::getword_mp3_list(QStringList wordstrlist, QString outpath)
{
    this->wordstrlist=wordstrlist;
    mp3outpath=outpath;

    getmpslist();
    return;
}

void words::getmpslist()
{
    QString word_str=wordstrlist[biaoshi];
    qDebug()<<"biaoshi"<<biaoshi;

    word_str=word_str.remove("\n");
    word_str=word_str.remove("\n\r");
    word_str=word_str.remove("\r\n");
    word_str=word_str.remove("\r");

    QString url = QString("http://dict.youdao.com/dictvoice?type=1&audio=%1").arg(word_str);     //获取url
//    qDebug()<<url;
    //1. 创建一个请求
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    //2.管理器
    //3.链接请求结束(保存mp3)
    //4.发送
    managerlist.get(request);

    return;
}


QString words::getword_trans(QString word)
{

    return  wordtrans.translate(word);
}

QString words::getword_trans_chi_en(QString chi,bool mohu)
{

    return wordtrans.translate_chi_en(chi, mohu);
}

QString words::getword_trans_en_chi(QString en,int leftlen)
{
    return wordtrans.translate_en_chi(en, leftlen);
}

QString words::entochi(QString word, bool mohu)
{
    return wordtrans.entochi( word,  mohu);

}

QStringList words::trans_entochi(QString word, int leftlen, int need)
{
    QStringList list=wordtrans.trans_entochi( word,  leftlen,  need);

    return list;
}
QString words::get_word_pictrue(QString word )
{

    word=chi_zhuan_http(word);

    //qDebug()<<word;

    return QString("https://cn.bing.com/images/search?q=%1&form=HDRSC2&first=1&tsc=ImageBasicHover").arg(word);
}

QString words::chi_zhuan_http(QString chi)
{

    QString strInput = chi;
        if(strInput.isEmpty())
        {
            return "word 是空";
        }
        QTextCodec * codecGB2312 = QTextCodec::codecForName("ut-f8");
        QByteArray byteArrayGB2312 = codecGB2312->fromUnicode(strInput);
        QByteArray byteArrayPercentEncoded = byteArrayGB2312.toPercentEncoding();
        chi=(QString(byteArrayPercentEncoded));

        return chi;
}

void words::getword_picture(QString word, QString outfilename)
{
    //一个一个来
    if(isworking==true)
        return;

    isworking=true;

    QString word_str=word;
    word_str=word_str.remove("\n");
    word_str=word_str.remove("\n\r");
    word_str=word_str.remove("\r\n");
    word_str=word_str.remove("\r");

    picturename=outfilename;
    word_draw=word_str;
    //https://source.unsplash.com/featured/?{KEYWORD},{KEYWORD}
    //https://source.unsplash.com/1600x900/?nature,water

    QString url = QString("https://source.unsplash.com/1280x720/?%1").arg(word_str);     //获取url
//    qDebug()<<url;
    //1. 创建一个请求
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    //2.管理器
    //3.链接请求结束(保存mp3)
    //4.发送
    manager_pic.get(request);

    return;

}

void words::savewordpic(QNetworkReply*reply)
{

//<html><body>You are being <a href="https://images.unsplash.com/photo-1529579134665-75dfc9c5ccef?crop=entropy&amp;cs=tinysrgb&amp;fit=max&amp;fm=jpg&amp;ixid=MnwxfDB8MXxyYW5kb218MHx8Y2hpbmF8fHx8fHwxNjQ5NTk2MzY2&amp;ixlib=rb-1.2.1&amp;q=80&amp;utm_campaign=api-credit&amp;utm_medium=referral&amp;utm_source=unsplash_source&amp;w=1080">redirected</a>.</body></html>
    QByteArray bytes = reply->readAll();  //获取字节

    QString str=bytes.data();

    if(str.isEmpty())
    {
        QMessageBox customMsgBox;
        //添加自定义按钮
        QPushButton*ok = customMsgBox.addButton("确定",QMessageBox::ActionRole);
        customMsgBox.setWindowTitle("请查看是否联网(图片http)");//设置窗口标题
        customMsgBox.setText(str);
        qDebug()<<str;
        customMsgBox.exec();//显示窗口
        //定义对话框响应事件
        if(customMsgBox.clickedButton()==ok){

             qDebug()<<"数据为空,是否联网,是否开启权限";
        }

        //没有联网也要发信号,让按键可按
        emit singal_getwordpic(false);

        return;
    }

    str=str.replace("\"","|");

    str=str.split(QRegExp("[|]")  )[1];

    //qDebug()<<"http:"<<str;
    //1. 创建一个请求
    QNetworkRequest request;
    request.setUrl(QUrl(str));
    //2.管理器
    //3.链接请求结束(保存mp3)
    //4.发送
    manager_pic_http.get(request);

    //delete[] reply; 为什么此处delete出错

    return;


}

void words::getwordpic_http(QNetworkReply*reply)
{
    QByteArray bytes = reply->readAll();  //获取字节
    QString str=bytes.data();
    if(str.isEmpty())
    {
        QMessageBox customMsgBox;
        //添加自定义按钮
        QPushButton*ok = customMsgBox.addButton("确定",QMessageBox::ActionRole);
        customMsgBox.setWindowTitle("请查看是否联网(图片)");//设置窗口标题
        customMsgBox.setText(str);
         qDebug()<<str;
        //customMsgBox.setIconPixmap(QPixmap("01.png"))//设置图标
        customMsgBox.exec();//显示窗口
        //定义对话框响应事件
        if(customMsgBox.clickedButton()==ok){

             qDebug()<<"数据为空,是否联网,是否开启权限";
        }

        //没有联网也要发信号,让按键可按
        emit singal_getwordpic(false);

        return;
    }
    QFile file(picturename);

    if(file.open(QIODevice::ReadWrite))
    {

        file.write(bytes);
        file.close();
    }
    else {
        qDebug()<<QString("void words::savewordpic(QNetworkReply*reply) ")<<(picturename);
    }


    //delete[] reply;为什么此处delete出错

    //画单词到图片上
    QImage img(picturename);
    //img=img.scaled(1280,720);
    QPainter painter(&img);
    QPen pen((255,255,255));//字色
    painter.setPen(pen);

    QFont font("楷体",false);//字体
    font.setPixelSize(img.height()/5);
    font.setBold(false);
    painter.setFont(font);

    QRect rect(0,0,img.width(),img.height()/2);
    painter.drawText(rect,Qt::AlignCenter,word_draw);

    rect.setRect(0,img.height()/2,img.width(),img.height()/2);
    pen.setColor(QColor((0,0,0)));
    painter.setPen(pen);
    font.setPixelSize(img.height()/8);
    painter.setFont(font);
    painter.drawText(rect,Qt::AlignCenter,word_draw);

    img.save(picturename);

    emit singal_getwordpic(true);
    isworking=false;

    return;

}

void words::Playback_of_audio(QString filename)
{
    QFileInfo info(filename);
    if(info.isFile()==false)
    {
        qDebug()<<"void words::Playback_of_audio(QString filename) 文件不存在";
        return;
    }

    QFile file(filename);
    if(file.open(QIODevice::ReadOnly))
    {
        if(file.readAll().isEmpty())
        {
            qDebug()<<"文件为空";

            file.close();
            return;
        }
    }

    //清除列表
    playlist.clear();

    playlist.setPlaybackMode(QMediaPlaylist::Loop);//设置循环模式
    player.setPlaylist(&playlist);//获取将播放列表要播放的文件

    playlist.addMedia(QUrl::fromLocalFile(filename));

    player.play();
}

void words::play_audio(QString filename)
{
    qDebug()<<"void words::play_audio(QString filename)";
    QFileInfo info(filename);
    if(info.isFile()==false)
    {
        qDebug()<<"void words::play_audio(QString filename) 文件不存在";
        return;
    }

    QFile file(filename);
    if(file.open(QIODevice::ReadOnly))
    {
        if(file.readAll().isEmpty())
        {
            qDebug()<<"文件为空";

            file.close();
            return;
        }


        file.close();
    }

    //清除所有列表
    playlist.clear();

    player.setMedia(QUrl::fromLocalFile(filename));

    player.play();
}

void words::play_audio_by_jiange(QString filename, int msec)
{
    QFileInfo info(filename);
    if(info.isFile()==false)
    {
        qDebug()<<"void words::play_audio_by_jiange(QString filename, int msec) 文件不存在";
        return;
    }

    audiofilename=filename;
    jiange_msec=msec;

    play_audio(filename);

    timer_jiange.start(msec);


}

void words::start_audio()
{
    player.play();
}

void words::Pause_audio()
{
    player.pause();
}

void words::stop_audio()
{
    player.stop();
    //间隔播放的停止
    //间隔模式
    stop_jiangetime=true;

}

int words::get_aplay_state()
{
    //player->state()会返回当前播放器的状态，:PlayingState(值为1)为播放中状态，对应的还有StoppedState（值为0）空闲状态，PausedState（值为2）播放暂停状态。

    return player.state();

}

QStringList words::syllabification(QString word)
{
    QStringList returnlist;

    //原音字母位置
    QStringList yuanyin;
    yuanyin.append("a");yuanyin.append("e");yuanyin.append("i");yuanyin.append("o");yuanyin.append("u");
    yuanyin.append("A");yuanyin.append("E");yuanyin.append("I");yuanyin.append("O");yuanyin.append("U");

    QByteArray ba2;
    ba2.append(word);
    char*cha=ba2.data();
    QStringList yunyinpos;
    for(int i=0;i<word.length();i++)
    {
        QString str( cha[i] );
        if(yuanyin.contains((str)))
            yunyinpos.append(QString::number(i));
    }

    //原音两个字母之间
    QStringList huanfenpos;
    for(int i=0;i<yunyinpos.length()-1;i++)
    {
        //两个原音字母是否紧挨着
        int posbef=QString( yunyinpos[i]).toInt();
        int posaft=QString( yunyinpos[i+1]).toInt();

        if(posbef+1==posaft)
        {
            continue;
        }
        else
        {
            //判断中间的辅音字母

            //两元音字母之间有一个辅音字母时，辅音字母归后一音节。

            int fuyinnum=posaft-posbef;
            if(fuyinnum==2)//一个辅音
                huanfenpos.append(QString(posaft-1));
            else if(fuyinnum==3)//两个辅音
                huanfenpos.append(QString(posaft-1));
            else
                huanfenpos.append(QString(posbef+2));

        }


    }

        if(huanfenpos.isEmpty())
            returnlist.append(word);
        else
        {
            //得到划分的单词块
            QString str[huanfenpos.length()+1];
            QByteArray ba2;
            ba2.append(word);
            char*cha=ba2.data();
            for(int i=0,j=0;i<word.length();i++)
            {
                if(huanfenpos.contains(QString(i)))
                    j++;
                QString st( cha[i] );
                str[j].append(cha[i]);


            }
            for(int i=0;i<huanfenpos.length()+1;i++)
                returnlist.append(str[i]);
        }



    return returnlist;
}

void words::ReplyFinished(QNetworkReply *reply)
{
    //"E MediaPlayerNative: error (1, -2147483648)
    //E MediaPlayer: Error (1,-2147483648)"
    //闪退
    //原因是没联网,mp3不是返回的数据,MediaPlayerNative 无法播放

    qDebug()<<"void words::ReplyFinished(QNetworkReply *reply)";
    QByteArray bytes = reply->readAll();  //获取字节
    QString str=bytes.data();

    if(str.isEmpty())
    {
        QMessageBox customMsgBox;
        //添加自定义按钮
        QPushButton*ok = customMsgBox.addButton("确定",QMessageBox::ActionRole);
        customMsgBox.setWindowTitle("请查看是否联网(音频)");//设置窗口标题
        //customMsgBox.setIconPixmap(QPixmap("01.png"))//设置图标
        customMsgBox.exec();//显示窗口
        //定义对话框响应事件
        if(customMsgBox.clickedButton()==ok){

             qDebug()<<"数据为空,是否联网,是否开启权限";
        }


        return;
    }

    QFile file(wordmp3_filename);

    if(file.open(QIODevice::ReadWrite))
    {

        file.write(bytes);
        file.close();
    }
    else {
        qDebug()<<QString("void words::ReplyFinished(QNetworkReply *reply) 打开音频输入文件错误 %1").arg(wordmp3_filename);
    }




    //delete[] reply;

    emit signal_getword_mp3();

    return;
}

void words::ReplyFinishedlist(QNetworkReply *reply)
{

    QString filename=mp3outpath+"/"+wordstrlist[biaoshi]+".mp3";

    QByteArray bytes = reply->readAll();  //获取字节
    QString str=bytes.data();

    if(str.isEmpty())
    {
        QMessageBox customMsgBox;
        //添加自定义按钮
        QPushButton*ok = customMsgBox.addButton("确定",QMessageBox::ActionRole);
        customMsgBox.setWindowTitle("请查看是否联网(批量音频)");//设置窗口标题
        //customMsgBox.setIconPixmap(QPixmap("01.png"))//设置图标
        customMsgBox.exec();//显示窗口
        //定义对话框响应事件
        if(customMsgBox.clickedButton()==ok){

             qDebug()<<"数据为空,是否联网,是否开启权限";
        }


        return;
    }


    QFile file(filename);
    QFileInfo info(filename);
    if(info.isFile()==false)
    {
        if(file.open(QIODevice::ReadWrite))
        {

            file.write(bytes);
            file.close();
        }
        else {
            qDebug()<<QString("void words::ReplyFinishedlist(QNetworkReply *reply)%1").arg(wordmp3_filename);
        }

    }
    else
    {
        qDebug()<<"mp3存在";
    }

    if(biaoshi==wordstrlist.count()-1)
    {
        qDebug()<<"批量获取单词结束";
        emit singal_getmp3list();

        biaoshi=0;
        wordstrlist.clear();
        //delete []reply;
        return;
    }

    timer_mp3list.start(2);


    //delete[] reply;

}

void words::slot_jiange()
{
    qDebug()<<"connect(&timer_jiange,&QTimer::timeout,this,&words::slot_jiange);"<<"bool"<<stop_jiangetime<<"time"<<jiange_msec;
    timer_jiange.stop();

    //不在循环
    if (stop_jiangetime==true)
    {
        //重新=false
        stop_jiangetime=false;

        timer_jiange.stop();
        return ;
    }




    play_audio(audiofilename);
    qDebug()<<"void words::slot_jiange() play_audio(audiofilename);";
    timer_jiange.start(jiange_msec);


}

bool words::IsEnglish(QString &qstrSrc)
{
    QByteArray ba = qstrSrc.toLatin1();
    const char *s = ba.data();
    bool bret = true;
    while(*s)
    {
        if((*s>='A' && *s<='Z') || (*s>='a' && *s<='z'))
        {

        }
        else
        {
            bret = false;
            break;
        }
        s++;
    }
    return bret;
}

