#ifndef WORDS_H
#define WORDS_H

#include <QObject>
#include<qfiledialog.h>
//#include<QAxObject>
#include<qdebug.h>
#include<QNetworkAccessManager>
#include<qnetworkrequest.h>
#include<qnetworkreply.h>
#include<QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include<word_trans.h>

#include<QtMultimedia/QMediaPlayer>
#include<QtMultimedia/QMediaPlaylist>
#include<qtimer.h>
#include<QTextCodec>
#include<qimage.h>
#include<QPainter>
#include<QPen>
class words : public QObject
{
    Q_OBJECT
public:
    explicit words(QObject *parent = nullptr);


    //从excel 得到单词
    //QStringList getwords_ForExcel(QString excelfilename);

    //从txt文件导入单词
    QStringList getwords_bytxt(QString txtfilename);

    //获取单词发音
    void getword_mp3(QString word_str,QString outfilename);//一个

    void getword_mp3_list(QStringList wordstrlist,QString outpath);
    void getmpslist();

    //是否是英文
    bool IsEnglish(QString &qstrSrc);

    //单词翻译
    QString getword_trans(QString word);
    QString getword_trans_chi_en(QString chi,bool mohu);
    QString getword_trans_en_chi(QString en,int leftlen);
    QString entochi(QString word, bool mohu);
    QStringList trans_entochi(QString word,int leftlen ,int need);

    //获取图片网址
    QString get_word_pictrue(QString word);
    QString chi_zhuan_http(QString chi);

    //获取相关图片
    void getword_picture(QString word,QString outfilename);
    void savewordpic(QNetworkReply*reply);
    void getwordpic_http(QNetworkReply*reply);

    //播放音频
    //"E MediaPlayerNative: error (1, -2147483648)
    //E MediaPlayer: Error (1,-2147483648)"
    //闪退
    //原因是没联网,mp3不是返回的数据,MediaPlayerNative 无法播放

    void Playback_of_audio(QString filename);//循环
    void play_audio(QString filename);//单次
    void play_audio_by_jiange(QString filename,int msec);

    void start_audio();
    //暂停
    void Pause_audio();
    //停止
    void stop_audio();
    //播放状态
    int get_aplay_state();

    //单词音节划分
    QStringList syllabification(QString word);


signals:
    void signal_getword_mp3();
    void singal_getmp3list();
    void singal_getwordpic(bool success);

private slots:
    //3.链接请求结束(保存mp3)
    void ReplyFinished(QNetworkReply *reply);
    void ReplyFinishedlist(QNetworkReply *reply);

    //间隔
    void slot_jiange();

private:
     QNetworkAccessManager manager;
     QNetworkAccessManager managerlist;
     QNetworkAccessManager manager_pic;
     QNetworkAccessManager manager_pic_http;


     //播放音乐
     QMediaPlayer player;
     QMediaPlaylist playlist;

     //词典
     word_trans wordtrans;

     //单词mp3文件
     QString wordmp3_filename;
     //
     QStringList wordstrlist;
     QString mp3outpath;
     int biaoshi=0;
    QTimer timer_mp3list;
     //定时器 播放间隔
     QTimer timer_jiange;
     QString audiofilename;
     int jiange_msec;

     //单词图片
     QString picturename;
     QString word_draw;
     bool isworking=false;
public:
     bool stop_jiangetime=false;


};

#endif // WORDS_H
