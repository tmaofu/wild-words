#include "word_trans.h"
#include "stdio.h"
#include "widget.h"
#include <QMessageBox>
#include<qfile.h>

word_trans::word_trans(QObject *parent) : QObject(parent)
{
    //加载资源文件

    QString filename=QString(":/chidian");
    QFileInfo info(filename);
    if(info.isFile()==0)
    {
        qDebug()<<"词典不存在";
        return;
    }
    //qDebug()<<"info.fileName:"<<info.filePath();

    // exepath=QDir::currentPath();

    //forandroid
    //exepath="/storage/emulated/0/wild_word狂野单词";
    //系系统目录
    exepath=QDir().currentPath();

    QString str=exepath+"/chidian";

    QFile filenew(str),fileold(filename);

    if(filenew.open(QIODevice::ReadWrite)==true&&fileold.open(QIODevice::ReadOnly)==true)
    {


        QString str=fileold.readAll();
        //qDebug()<<"str"<<str;
        filenew.write( str.toUtf8() );

        filenew.close();
        fileold.close();
    }

    //dict map
    CreateDict(&mapDict,&mapDict_chi_en);

}

//打开字典文件，并读取文件内容
void word_trans::CreateDict(QMap<QString, QString>  *myDict,QMap<QString, QString>  *myDict_chi_en)
{

    QString filename=exepath+"/chidian";

    QString word, inter;

    QFile fp(filename);
    if (!fp.open(QIODevice::ReadWrite)) {
        QMessageBox::information(nullptr,
            tr("警告"),
            tr("打开词库失败!"));

        return;
    }
    else
    {
        while (fp.atEnd()==false)
        {
            // 去除换行符
            // 去除换行符
            // 去除换行符
            word=fp.readLine();
            inter=fp.readLine();
            word.remove("\n");
            word.remove("\r");
            word.remove("\n\r");
            word.remove("\r\n");

            inter.remove("\n");
            inter.remove("\r");
            inter.remove("\n\r");
            inter.remove("\r\n");

            (*myDict)[word]=inter;//英中
            (*myDict_chi_en)[inter]=word;
        }


        fp.close();
    }



}

QString word_trans::translate(QString word)
{

    word=word.remove("\n");
    word=word.remove("\n\r");
    word=word.remove("\r\n");
    word=word.remove("\r");

    if(word.isEmpty())
    {
        qDebug()<<"单词为空";
        return "单词为空";
    }


    QString str;
    query=word;
    if(mapDict.find(query) != mapDict.end())
    {
        str=(mapDict[query]);
    }
    else
    {
       str=("Not found");
    }
    //qDebug()<<str;

    return str;

}

QString word_trans::translate_chi_en(QString chi,bool mohu)
{
    QString word=chi;

    word=word.remove("\n");
    word=word.remove("\n\r");
    word=word.remove("\r\n");
    word=word.remove("\r");

    if(word.isEmpty())
    {
        qDebug()<<"单词为空";
        return "单词为空";
    }

    //遍历
    QString returnstr;
    QMap<QString, QString>::const_iterator i = mapDict_chi_en.constBegin();
    while (i != mapDict_chi_en.constEnd())
    {

        if(mohu==true)
        {
            if(i.key().contains(word))
            {
                returnstr.append(i.key()+"\n"+i.value()+"\n\n");

            }
        }
        else
        {
            if(i.key()==word)
            {
                returnstr.append(i.key()+"\n"+i.value());
                break;
            }
        }
        ++i;
    }

    if(returnstr.isEmpty())
        returnstr="没有找到";

    return returnstr;

}


QString word_trans::translate_en_chi(QString word,bool mohu)
{

    word=word.remove("\n");
    word=word.remove("\n\r");
    word=word.remove("\r\n");
    word=word.remove("\r");

    if(word.isEmpty())
    {
        qDebug()<<"单词为空";
        return "单词为空";
    }

    //遍历
    QString returnstr;
    QMap<QString, QString>::const_iterator i = mapDict.constBegin();
    while (i != mapDict.constEnd())
    {

        if(mohu==true)
        {
            if(i.key().left(word.length())==word)
            {
                returnstr.append(i.key()+"\n"+i.value()+"\n\n");

            }
        }
        else
        {
            if(i.key()==word)
            {
                returnstr.append(i.key()+"\n"+i.value());
                break;
            }

        }
        ++i;
    }

    if(returnstr.isEmpty())
        returnstr="没有找到";


    return returnstr;

}

QString word_trans::entochi(QString word, int leftlen)
{

    word=word.remove("\n");
    word=word.remove("\n\r");
    word=word.remove("\r\n");
    word=word.remove("\r");

    if(word.isEmpty())
    {
        qDebug()<<"单词为空";
        return "单词为空";
    }

    //遍历
    QString returnstr;
    QMap<QString, QString>::const_iterator i = mapDict.constBegin();
    while (i != mapDict.constEnd())
    {

        if(i.key()!=word)
            if(i.key().left( leftlen)==word.left(leftlen))
            {

                returnstr.append(i.value()+"|");

            }

        ++i;
    }

    if (returnstr.isEmpty())
        returnstr.append("暂无|");


    return returnstr;
}

QStringList word_trans::trans_entochi(QString word , int leftlen,int need)
{
    QStringList returnstr;

    word=word.remove("\n");
    word=word.remove("\n\r");
    word=word.remove("\r\n");
    word=word.remove("\r");

    if(word.isEmpty())
    {
        qDebug()<<"单词为空";
        returnstr.append("单词为空");
        return returnstr;
    }

    word=word.left(leftlen);

    //遍历
    int num=0;
    QMap<QString, QString>::const_iterator i = mapDict.constBegin();
    while (i != mapDict.constEnd())
    {

        if(i.key().left(word.length())==word)
        {
            returnstr.append(i.value());
            num++;

        }

        if(num>=need)
            break;
    }

    if(returnstr.isEmpty())
       for(int i=0;i<need;i++)
           returnstr.append("没有找到");

    if(returnstr.length()<need)
        for(int i=0;i<need-returnstr.length();i++)
            returnstr.append("没有找到");


    return returnstr;
}
