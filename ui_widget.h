/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout_3;
    QFrame *line_8;
    QLabel *label;
    QTextBrowser *textBrowser;
    QScrollArea *scrollArea_4;
    QWidget *scrollAreaWidgetContents_2;
    QGridLayout *gridLayout_24;
    QLabel *label_dongtai_word;
    QLabel *label_yingjie_huafen;
    QFrame *frame;
    QGridLayout *gridLayout_19;
    QWidget *widget_3;
    QGridLayout *gridLayout_6;
    QTabWidget *tabWidget_2;
    QWidget *tab_3;
    QGridLayout *gridLayout_4;
    QPushButton *pushButton_change_pic1;
    QLabel *label_pic1;
    QWidget *tab_4;
    QGridLayout *gridLayout_5;
    QLabel *label_pic2;
    QWidget *tab_9;
    QGridLayout *gridLayout_14;
    QPushButton *pushButton_3;
    QPushButton *pushButton_10;
    QPushButton *pushButton_14;
    QPushButton *pushButton_9;
    QCheckBox *checkBox_mohu;
    QLineEdit *lineEdit_chidian_inp;
    QFrame *line_15;
    QFrame *line_14;
    QLabel *label_11;
    QSpacerItem *verticalSpacer;
    QWidget *tab_6;
    QGridLayout *gridLayout_10;
    QDockWidget *dockWidget;
    QWidget *dockWidgetContents;
    QGridLayout *gridLayout_9;
    QListView *listView;
    QWidget *tab_8;
    QGridLayout *gridLayout_20;
    QTabWidget *tabWidget_3;
    QWidget *tab_13;
    QGridLayout *gridLayout_11;
    QListView *listView_huigu;
    QListView *listView_huigu_shiyi;
    QWidget *widget_2;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_lookshiyi;
    QPushButton *pushButton_15;
    QWidget *tab_14;
    QGridLayout *gridLayout_12;
    QWidget *widget_4;
    QGridLayout *gridLayout_2;
    QTextBrowser *textBrowser_cheshi;
    QLabel *label_cheshi_pic;
    QFrame *line_17;
    QLabel *label_danchi_cheshi;
    QScrollArea *scrollArea_2;
    QWidget *scrollAreaWidgetContents_3;
    QGridLayout *gridLayout_22;
    QRadioButton *radioButton_4;
    QRadioButton *radioButton_3;
    QRadioButton *radioButton_2;
    QRadioButton *radioButton;
    QPushButton *pushButton_13;
    QCheckBox *checkBox_stop_xunhuan_currlist;
    QPushButton *pushButton_16;
    QPushButton *pushButton_12;
    QPushButton *pushButton;
    QPushButton *pushButton_last;
    QPushButton *pushButton_wordslist;
    QPushButton *pushButton_next;
    QWidget *tab_7;
    QGridLayout *gridLayout_13;
    QFrame *line_10;
    QLabel *label_13;
    QFrame *line_7;
    QLabel *label_5;
    QPushButton *pushButton_2;
    QListView *listView_lishi_shuju;
    QLineEdit *lineEdit_edit_lishi_filename;
    QWidget *tab_2;
    QGridLayout *gridLayout_8;
    QScrollArea *scrollArea_5;
    QWidget *scrollAreaWidgetContents_5;
    QGridLayout *gridLayout_25;
    QSpinBox *spinBox_dongtai_msec;
    QLabel *label_12;
    QPushButton *openfile_bytxt;
    QPushButton *pushButton_5;
    QLabel *label_14;
    QLabel *label_3;
    QFrame *line_12;
    QSpinBox *spinBox_dongtai_jiange;
    QSpinBox *danchibofan_jiange;
    QDoubleSpinBox *doubleSpinBox_cheshi;
    QLabel *label_8;
    QFrame *line_4;
    QPushButton *pushButton_7;
    QLabel *label_4;
    QCheckBox *checkBox_xunhuan;
    QSpinBox *spinBox;
    QPushButton *pushButton_chengchiben;
    QFrame *line;
    QLabel *label_9;
    QFrame *line_6;
    QCheckBox *checkBox_huigu;
    QFrame *line_13;
    QPushButton *pushButton_11;
    QFrame *line_2;
    QSpinBox *spinBox_huigu;
    QLabel *label_2;
    QFrame *line_9;
    QPushButton *pushButton_4;
    QFrame *line_3;
    QLabel *label_10;
    QSpinBox *spinBox_bofang_liebao;
    QCheckBox *checkBox_dongtai_close;
    QFrame *line_5;
    QFrame *line_16;
    QWidget *tab_10;
    QGridLayout *gridLayout_16;
    QLabel *label_6;
    QTextEdit *textEdit;
    QFrame *frame_2;
    QGridLayout *gridLayout_15;
    QPushButton *pushButton_textedit_celer;
    QPushButton *pushButton_dele_jushuh;
    QPushButton *pushButton_dele_oushuh;
    QPushButton *pushButton_6;
    QPushButton *pushButton_daoru_text;
    QWidget *tab_5;
    QGridLayout *gridLayout_18;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_17;
    QWidget *widget;
    QGridLayout *gridLayout_7;
    QTextBrowser *textBrowser_2;
    QLabel *label_datapath;
    QLabel *label_7;
    QPushButton *pushButton_8;
    QLabel *label_show;
    QFrame *line_11;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QString::fromUtf8("Widget"));
        Widget->resize(513, 1204);
        QFont font;
        font.setFamily(QString::fromUtf8("\346\245\267\344\275\223"));
        font.setPointSize(14);
        Widget->setFont(font);
        Widget->setStyleSheet(QString::fromUtf8(""));
        gridLayout = new QGridLayout(Widget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        tabWidget = new QTabWidget(Widget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setMaximumSize(QSize(999999, 16777215));
        tabWidget->setStyleSheet(QString::fromUtf8(""));
        tabWidget->setTabPosition(QTabWidget::South);
        tabWidget->setTabShape(QTabWidget::Triangular);
        tabWidget->setIconSize(QSize(28, 28));
        tabWidget->setElideMode(Qt::ElideNone);
        tabWidget->setDocumentMode(false);
        tabWidget->setTabsClosable(false);
        tabWidget->setMovable(false);
        tabWidget->setTabBarAutoHide(false);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        gridLayout_3 = new QGridLayout(tab);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        line_8 = new QFrame(tab);
        line_8->setObjectName(QString::fromUtf8("line_8"));
        line_8->setFrameShadow(QFrame::Plain);
        line_8->setFrameShape(QFrame::HLine);

        gridLayout_3->addWidget(line_8, 1, 0, 1, 1);

        label = new QLabel(tab);
        label->setObjectName(QString::fromUtf8("label"));
        label->setTextFormat(Qt::PlainText);

        gridLayout_3->addWidget(label, 0, 0, 1, 1);

        textBrowser = new QTextBrowser(tab);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setMinimumSize(QSize(424, 180));
        textBrowser->setMaximumSize(QSize(999999, 999999));
        QFont font1;
        font1.setFamily(QString::fromUtf8("\346\245\267\344\275\223"));
        font1.setPointSize(28);
        textBrowser->setFont(font1);
        textBrowser->setTabChangesFocus(false);
        textBrowser->setReadOnly(true);
        textBrowser->setOpenExternalLinks(false);

        gridLayout_3->addWidget(textBrowser, 2, 0, 1, 1);

        scrollArea_4 = new QScrollArea(tab);
        scrollArea_4->setObjectName(QString::fromUtf8("scrollArea_4"));
        scrollArea_4->setMinimumSize(QSize(0, 0));
        scrollArea_4->setMaximumSize(QSize(16777215, 180));
        scrollArea_4->setWidgetResizable(true);
        scrollAreaWidgetContents_2 = new QWidget();
        scrollAreaWidgetContents_2->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_2"));
        scrollAreaWidgetContents_2->setGeometry(QRect(0, 0, 461, 85));
        gridLayout_24 = new QGridLayout(scrollAreaWidgetContents_2);
        gridLayout_24->setObjectName(QString::fromUtf8("gridLayout_24"));
        label_dongtai_word = new QLabel(scrollAreaWidgetContents_2);
        label_dongtai_word->setObjectName(QString::fromUtf8("label_dongtai_word"));
        label_dongtai_word->setMaximumSize(QSize(16777215, 999));
        QFont font2;
        font2.setFamily(QString::fromUtf8("\346\245\267\344\275\223"));
        font2.setPointSize(9);
        label_dongtai_word->setFont(font2);

        gridLayout_24->addWidget(label_dongtai_word, 1, 0, 1, 1);

        label_yingjie_huafen = new QLabel(scrollAreaWidgetContents_2);
        label_yingjie_huafen->setObjectName(QString::fromUtf8("label_yingjie_huafen"));
        label_yingjie_huafen->setMaximumSize(QSize(16777215, 999));
        label_yingjie_huafen->setFont(font2);

        gridLayout_24->addWidget(label_yingjie_huafen, 2, 0, 1, 1);

        scrollArea_4->setWidget(scrollAreaWidgetContents_2);

        gridLayout_3->addWidget(scrollArea_4, 3, 0, 1, 1);

        frame = new QFrame(tab);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMaximumSize(QSize(999999, 16777215));
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout_19 = new QGridLayout(frame);
        gridLayout_19->setObjectName(QString::fromUtf8("gridLayout_19"));
        widget_3 = new QWidget(frame);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        gridLayout_6 = new QGridLayout(widget_3);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        tabWidget_2 = new QTabWidget(widget_3);
        tabWidget_2->setObjectName(QString::fromUtf8("tabWidget_2"));
        tabWidget_2->setStyleSheet(QString::fromUtf8(""));
        tabWidget_2->setTabPosition(QTabWidget::South);
        tabWidget_2->setTabShape(QTabWidget::Triangular);
        tabWidget_2->setIconSize(QSize(28, 28));
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        gridLayout_4 = new QGridLayout(tab_3);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        pushButton_change_pic1 = new QPushButton(tab_3);
        pushButton_change_pic1->setObjectName(QString::fromUtf8("pushButton_change_pic1"));

        gridLayout_4->addWidget(pushButton_change_pic1, 2, 0, 1, 2);

        label_pic1 = new QLabel(tab_3);
        label_pic1->setObjectName(QString::fromUtf8("label_pic1"));
        label_pic1->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pic1, 1, 0, 1, 2);

        tabWidget_2->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        gridLayout_5 = new QGridLayout(tab_4);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        label_pic2 = new QLabel(tab_4);
        label_pic2->setObjectName(QString::fromUtf8("label_pic2"));
        label_pic2->setAlignment(Qt::AlignCenter);

        gridLayout_5->addWidget(label_pic2, 0, 0, 1, 1);

        tabWidget_2->addTab(tab_4, QString());
        tab_9 = new QWidget();
        tab_9->setObjectName(QString::fromUtf8("tab_9"));
        gridLayout_14 = new QGridLayout(tab_9);
        gridLayout_14->setObjectName(QString::fromUtf8("gridLayout_14"));
        pushButton_3 = new QPushButton(tab_9);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        gridLayout_14->addWidget(pushButton_3, 7, 0, 1, 2);

        pushButton_10 = new QPushButton(tab_9);
        pushButton_10->setObjectName(QString::fromUtf8("pushButton_10"));

        gridLayout_14->addWidget(pushButton_10, 4, 0, 1, 1);

        pushButton_14 = new QPushButton(tab_9);
        pushButton_14->setObjectName(QString::fromUtf8("pushButton_14"));

        gridLayout_14->addWidget(pushButton_14, 3, 0, 1, 1);

        pushButton_9 = new QPushButton(tab_9);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));

        gridLayout_14->addWidget(pushButton_9, 4, 1, 1, 1);

        checkBox_mohu = new QCheckBox(tab_9);
        checkBox_mohu->setObjectName(QString::fromUtf8("checkBox_mohu"));

        gridLayout_14->addWidget(checkBox_mohu, 3, 1, 1, 1);

        lineEdit_chidian_inp = new QLineEdit(tab_9);
        lineEdit_chidian_inp->setObjectName(QString::fromUtf8("lineEdit_chidian_inp"));

        gridLayout_14->addWidget(lineEdit_chidian_inp, 2, 0, 1, 2);

        line_15 = new QFrame(tab_9);
        line_15->setObjectName(QString::fromUtf8("line_15"));
        line_15->setFrameShadow(QFrame::Plain);
        line_15->setLineWidth(5);
        line_15->setFrameShape(QFrame::HLine);

        gridLayout_14->addWidget(line_15, 0, 0, 1, 2);

        line_14 = new QFrame(tab_9);
        line_14->setObjectName(QString::fromUtf8("line_14"));
        line_14->setFrameShadow(QFrame::Plain);
        line_14->setLineWidth(5);
        line_14->setFrameShape(QFrame::HLine);

        gridLayout_14->addWidget(line_14, 5, 0, 1, 2);

        label_11 = new QLabel(tab_9);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_11, 1, 0, 1, 2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_14->addItem(verticalSpacer, 6, 0, 1, 2);

        tabWidget_2->addTab(tab_9, QString());
        tab_6 = new QWidget();
        tab_6->setObjectName(QString::fromUtf8("tab_6"));
        gridLayout_10 = new QGridLayout(tab_6);
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        dockWidget = new QDockWidget(tab_6);
        dockWidget->setObjectName(QString::fromUtf8("dockWidget"));
        dockWidget->setMaximumSize(QSize(99999, 524287));
        dockWidget->setStyleSheet(QString::fromUtf8(""));
        dockWidget->setFloating(false);
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QString::fromUtf8("dockWidgetContents"));
        gridLayout_9 = new QGridLayout(dockWidgetContents);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        listView = new QListView(dockWidgetContents);
        listView->setObjectName(QString::fromUtf8("listView"));
        listView->setStyleSheet(QString::fromUtf8(""));

        gridLayout_9->addWidget(listView, 0, 0, 1, 1);

        dockWidget->setWidget(dockWidgetContents);

        gridLayout_10->addWidget(dockWidget, 0, 0, 1, 1);

        tabWidget_2->addTab(tab_6, QString());
        tab_8 = new QWidget();
        tab_8->setObjectName(QString::fromUtf8("tab_8"));
        gridLayout_20 = new QGridLayout(tab_8);
        gridLayout_20->setObjectName(QString::fromUtf8("gridLayout_20"));
        tabWidget_3 = new QTabWidget(tab_8);
        tabWidget_3->setObjectName(QString::fromUtf8("tabWidget_3"));
        tab_13 = new QWidget();
        tab_13->setObjectName(QString::fromUtf8("tab_13"));
        gridLayout_11 = new QGridLayout(tab_13);
        gridLayout_11->setObjectName(QString::fromUtf8("gridLayout_11"));
        listView_huigu = new QListView(tab_13);
        listView_huigu->setObjectName(QString::fromUtf8("listView_huigu"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("\346\245\267\344\275\223"));
        font3.setPointSize(16);
        listView_huigu->setFont(font3);
        listView_huigu->setStyleSheet(QString::fromUtf8(""));

        gridLayout_11->addWidget(listView_huigu, 0, 0, 1, 1);

        listView_huigu_shiyi = new QListView(tab_13);
        listView_huigu_shiyi->setObjectName(QString::fromUtf8("listView_huigu_shiyi"));
        listView_huigu_shiyi->setStyleSheet(QString::fromUtf8(""));

        gridLayout_11->addWidget(listView_huigu_shiyi, 0, 1, 1, 1);

        widget_2 = new QWidget(tab_13);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        horizontalLayout = new QHBoxLayout(widget_2);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pushButton_lookshiyi = new QPushButton(widget_2);
        pushButton_lookshiyi->setObjectName(QString::fromUtf8("pushButton_lookshiyi"));
        pushButton_lookshiyi->setIconSize(QSize(25, 25));

        horizontalLayout->addWidget(pushButton_lookshiyi);

        pushButton_15 = new QPushButton(widget_2);
        pushButton_15->setObjectName(QString::fromUtf8("pushButton_15"));

        horizontalLayout->addWidget(pushButton_15);


        gridLayout_11->addWidget(widget_2, 1, 0, 1, 2);

        tabWidget_3->addTab(tab_13, QString());
        tab_14 = new QWidget();
        tab_14->setObjectName(QString::fromUtf8("tab_14"));
        gridLayout_12 = new QGridLayout(tab_14);
        gridLayout_12->setObjectName(QString::fromUtf8("gridLayout_12"));
        widget_4 = new QWidget(tab_14);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        gridLayout_2 = new QGridLayout(widget_4);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        textBrowser_cheshi = new QTextBrowser(widget_4);
        textBrowser_cheshi->setObjectName(QString::fromUtf8("textBrowser_cheshi"));

        gridLayout_2->addWidget(textBrowser_cheshi, 0, 0, 1, 1);

        label_cheshi_pic = new QLabel(widget_4);
        label_cheshi_pic->setObjectName(QString::fromUtf8("label_cheshi_pic"));
        label_cheshi_pic->setMinimumSize(QSize(151, 140));

        gridLayout_2->addWidget(label_cheshi_pic, 1, 0, 1, 1);

        line_17 = new QFrame(widget_4);
        line_17->setObjectName(QString::fromUtf8("line_17"));
        line_17->setFrameShape(QFrame::HLine);
        line_17->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_17, 2, 0, 1, 1);

        label_danchi_cheshi = new QLabel(widget_4);
        label_danchi_cheshi->setObjectName(QString::fromUtf8("label_danchi_cheshi"));
        label_danchi_cheshi->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_danchi_cheshi, 3, 0, 1, 1);

        scrollArea_2 = new QScrollArea(widget_4);
        scrollArea_2->setObjectName(QString::fromUtf8("scrollArea_2"));
        scrollArea_2->setMinimumSize(QSize(0, 150));
        scrollArea_2->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 97, 151));
        gridLayout_22 = new QGridLayout(scrollAreaWidgetContents_3);
        gridLayout_22->setObjectName(QString::fromUtf8("gridLayout_22"));
        radioButton_4 = new QRadioButton(scrollAreaWidgetContents_3);
        radioButton_4->setObjectName(QString::fromUtf8("radioButton_4"));

        gridLayout_22->addWidget(radioButton_4, 3, 0, 1, 1);

        radioButton_3 = new QRadioButton(scrollAreaWidgetContents_3);
        radioButton_3->setObjectName(QString::fromUtf8("radioButton_3"));

        gridLayout_22->addWidget(radioButton_3, 2, 0, 1, 1);

        radioButton_2 = new QRadioButton(scrollAreaWidgetContents_3);
        radioButton_2->setObjectName(QString::fromUtf8("radioButton_2"));

        gridLayout_22->addWidget(radioButton_2, 1, 0, 1, 1);

        radioButton = new QRadioButton(scrollAreaWidgetContents_3);
        radioButton->setObjectName(QString::fromUtf8("radioButton"));

        gridLayout_22->addWidget(radioButton, 0, 0, 1, 1);

        scrollArea_2->setWidget(scrollAreaWidgetContents_3);

        gridLayout_2->addWidget(scrollArea_2, 4, 0, 1, 1);


        gridLayout_12->addWidget(widget_4, 0, 0, 1, 1);

        tabWidget_3->addTab(tab_14, QString());

        gridLayout_20->addWidget(tabWidget_3, 0, 0, 1, 1);

        tabWidget_2->addTab(tab_8, QString());

        gridLayout_6->addWidget(tabWidget_2, 2, 0, 1, 1);


        gridLayout_19->addWidget(widget_3, 0, 0, 1, 2);

        pushButton_13 = new QPushButton(frame);
        pushButton_13->setObjectName(QString::fromUtf8("pushButton_13"));

        gridLayout_19->addWidget(pushButton_13, 1, 0, 2, 1);

        checkBox_stop_xunhuan_currlist = new QCheckBox(frame);
        checkBox_stop_xunhuan_currlist->setObjectName(QString::fromUtf8("checkBox_stop_xunhuan_currlist"));

        gridLayout_19->addWidget(checkBox_stop_xunhuan_currlist, 1, 1, 1, 1);

        pushButton_16 = new QPushButton(frame);
        pushButton_16->setObjectName(QString::fromUtf8("pushButton_16"));

        gridLayout_19->addWidget(pushButton_16, 2, 1, 2, 1);

        pushButton_12 = new QPushButton(frame);
        pushButton_12->setObjectName(QString::fromUtf8("pushButton_12"));

        gridLayout_19->addWidget(pushButton_12, 3, 0, 1, 1);

        pushButton = new QPushButton(frame);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setIconSize(QSize(25, 25));

        gridLayout_19->addWidget(pushButton, 4, 0, 1, 1);

        pushButton_last = new QPushButton(frame);
        pushButton_last->setObjectName(QString::fromUtf8("pushButton_last"));
        pushButton_last->setIconSize(QSize(25, 25));
        pushButton_last->setAutoDefault(true);
        pushButton_last->setFlat(false);

        gridLayout_19->addWidget(pushButton_last, 4, 1, 1, 1);

        pushButton_wordslist = new QPushButton(frame);
        pushButton_wordslist->setObjectName(QString::fromUtf8("pushButton_wordslist"));
        pushButton_wordslist->setIconSize(QSize(25, 25));

        gridLayout_19->addWidget(pushButton_wordslist, 5, 0, 1, 1);

        pushButton_next = new QPushButton(frame);
        pushButton_next->setObjectName(QString::fromUtf8("pushButton_next"));
        pushButton_next->setIconSize(QSize(25, 25));

        gridLayout_19->addWidget(pushButton_next, 5, 1, 1, 1);

        pushButton->raise();
        widget_3->raise();
        pushButton_wordslist->raise();
        pushButton_last->raise();
        pushButton_13->raise();
        pushButton_16->raise();
        pushButton_12->raise();
        checkBox_stop_xunhuan_currlist->raise();
        pushButton_next->raise();

        gridLayout_3->addWidget(frame, 5, 0, 1, 1);

        tabWidget->addTab(tab, QString());
        tab_7 = new QWidget();
        tab_7->setObjectName(QString::fromUtf8("tab_7"));
        gridLayout_13 = new QGridLayout(tab_7);
        gridLayout_13->setObjectName(QString::fromUtf8("gridLayout_13"));
        line_10 = new QFrame(tab_7);
        line_10->setObjectName(QString::fromUtf8("line_10"));
        line_10->setFrameShadow(QFrame::Plain);
        line_10->setLineWidth(5);
        line_10->setFrameShape(QFrame::HLine);

        gridLayout_13->addWidget(line_10, 3, 0, 1, 1);

        label_13 = new QLabel(tab_7);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout_13->addWidget(label_13, 6, 0, 1, 1);

        line_7 = new QFrame(tab_7);
        line_7->setObjectName(QString::fromUtf8("line_7"));
        line_7->setFrameShadow(QFrame::Plain);
        line_7->setLineWidth(5);
        line_7->setFrameShape(QFrame::HLine);

        gridLayout_13->addWidget(line_7, 0, 0, 1, 1);

        label_5 = new QLabel(tab_7);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_13->addWidget(label_5, 1, 0, 1, 1);

        pushButton_2 = new QPushButton(tab_7);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        gridLayout_13->addWidget(pushButton_2, 8, 0, 1, 1);

        listView_lishi_shuju = new QListView(tab_7);
        listView_lishi_shuju->setObjectName(QString::fromUtf8("listView_lishi_shuju"));
        listView_lishi_shuju->setStyleSheet(QString::fromUtf8(""));

        gridLayout_13->addWidget(listView_lishi_shuju, 5, 0, 1, 1);

        lineEdit_edit_lishi_filename = new QLineEdit(tab_7);
        lineEdit_edit_lishi_filename->setObjectName(QString::fromUtf8("lineEdit_edit_lishi_filename"));

        gridLayout_13->addWidget(lineEdit_edit_lishi_filename, 7, 0, 1, 1);

        tabWidget->addTab(tab_7, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        gridLayout_8 = new QGridLayout(tab_2);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        scrollArea_5 = new QScrollArea(tab_2);
        scrollArea_5->setObjectName(QString::fromUtf8("scrollArea_5"));
        scrollArea_5->setWidgetResizable(true);
        scrollAreaWidgetContents_5 = new QWidget();
        scrollAreaWidgetContents_5->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_5"));
        scrollAreaWidgetContents_5->setGeometry(QRect(0, 0, 461, 1092));
        gridLayout_25 = new QGridLayout(scrollAreaWidgetContents_5);
        gridLayout_25->setObjectName(QString::fromUtf8("gridLayout_25"));
        spinBox_dongtai_msec = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_dongtai_msec->setObjectName(QString::fromUtf8("spinBox_dongtai_msec"));
        spinBox_dongtai_msec->setMinimum(1);
        spinBox_dongtai_msec->setMaximum(99999);
        spinBox_dongtai_msec->setValue(1000);

        gridLayout_25->addWidget(spinBox_dongtai_msec, 14, 0, 1, 1);

        label_12 = new QLabel(scrollAreaWidgetContents_5);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout_25->addWidget(label_12, 13, 0, 1, 1);

        openfile_bytxt = new QPushButton(scrollAreaWidgetContents_5);
        openfile_bytxt->setObjectName(QString::fromUtf8("openfile_bytxt"));

        gridLayout_25->addWidget(openfile_bytxt, 23, 0, 1, 1);

        pushButton_5 = new QPushButton(scrollAreaWidgetContents_5);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));

        gridLayout_25->addWidget(pushButton_5, 33, 0, 1, 1);

        label_14 = new QLabel(scrollAreaWidgetContents_5);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout_25->addWidget(label_14, 18, 0, 1, 1);

        label_3 = new QLabel(scrollAreaWidgetContents_5);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setTextFormat(Qt::PlainText);
        label_3->setAlignment(Qt::AlignCenter);

        gridLayout_25->addWidget(label_3, 1, 0, 1, 1);

        line_12 = new QFrame(scrollAreaWidgetContents_5);
        line_12->setObjectName(QString::fromUtf8("line_12"));
        line_12->setFrameShape(QFrame::HLine);
        line_12->setFrameShadow(QFrame::Sunken);

        gridLayout_25->addWidget(line_12, 12, 0, 1, 1);

        spinBox_dongtai_jiange = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_dongtai_jiange->setObjectName(QString::fromUtf8("spinBox_dongtai_jiange"));
        spinBox_dongtai_jiange->setMinimum(1);
        spinBox_dongtai_jiange->setMaximum(99999);
        spinBox_dongtai_jiange->setValue(5000);

        gridLayout_25->addWidget(spinBox_dongtai_jiange, 15, 0, 1, 1);

        danchibofan_jiange = new QSpinBox(scrollAreaWidgetContents_5);
        danchibofan_jiange->setObjectName(QString::fromUtf8("danchibofan_jiange"));
        danchibofan_jiange->setMinimum(500);
        danchibofan_jiange->setMaximum(99999);
        danchibofan_jiange->setValue(1500);

        gridLayout_25->addWidget(danchibofan_jiange, 5, 0, 1, 1);

        doubleSpinBox_cheshi = new QDoubleSpinBox(scrollAreaWidgetContents_5);
        doubleSpinBox_cheshi->setObjectName(QString::fromUtf8("doubleSpinBox_cheshi"));
        doubleSpinBox_cheshi->setMaximum(1.000000000000000);
        doubleSpinBox_cheshi->setValue(0.400000000000000);

        gridLayout_25->addWidget(doubleSpinBox_cheshi, 11, 0, 1, 1);

        label_8 = new QLabel(scrollAreaWidgetContents_5);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_25->addWidget(label_8, 10, 0, 1, 1);

        line_4 = new QFrame(scrollAreaWidgetContents_5);
        line_4->setObjectName(QString::fromUtf8("line_4"));
        line_4->setFrameShadow(QFrame::Plain);
        line_4->setLineWidth(5);
        line_4->setFrameShape(QFrame::HLine);

        gridLayout_25->addWidget(line_4, 22, 0, 1, 1);

        pushButton_7 = new QPushButton(scrollAreaWidgetContents_5);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));

        gridLayout_25->addWidget(pushButton_7, 32, 0, 1, 1);

        label_4 = new QLabel(scrollAreaWidgetContents_5);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_25->addWidget(label_4, 4, 0, 1, 1);

        checkBox_xunhuan = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_xunhuan->setObjectName(QString::fromUtf8("checkBox_xunhuan"));
        checkBox_xunhuan->setChecked(true);
        checkBox_xunhuan->setTristate(false);

        gridLayout_25->addWidget(checkBox_xunhuan, 3, 0, 1, 1);

        spinBox = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        spinBox->setMaximum(280);
        spinBox->setValue(70);

        gridLayout_25->addWidget(spinBox, 30, 0, 1, 1);

        pushButton_chengchiben = new QPushButton(scrollAreaWidgetContents_5);
        pushButton_chengchiben->setObjectName(QString::fromUtf8("pushButton_chengchiben"));

        gridLayout_25->addWidget(pushButton_chengchiben, 24, 0, 1, 1);

        line = new QFrame(scrollAreaWidgetContents_5);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShadow(QFrame::Plain);
        line->setLineWidth(5);
        line->setFrameShape(QFrame::HLine);

        gridLayout_25->addWidget(line, 20, 0, 1, 1);

        label_9 = new QLabel(scrollAreaWidgetContents_5);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setAlignment(Qt::AlignCenter);

        gridLayout_25->addWidget(label_9, 27, 0, 1, 1);

        line_6 = new QFrame(scrollAreaWidgetContents_5);
        line_6->setObjectName(QString::fromUtf8("line_6"));
        line_6->setFrameShape(QFrame::HLine);
        line_6->setFrameShadow(QFrame::Sunken);

        gridLayout_25->addWidget(line_6, 9, 0, 1, 1);

        checkBox_huigu = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_huigu->setObjectName(QString::fromUtf8("checkBox_huigu"));
        checkBox_huigu->setChecked(true);

        gridLayout_25->addWidget(checkBox_huigu, 7, 0, 1, 1);

        line_13 = new QFrame(scrollAreaWidgetContents_5);
        line_13->setObjectName(QString::fromUtf8("line_13"));
        line_13->setFrameShadow(QFrame::Plain);
        line_13->setLineWidth(5);
        line_13->setFrameShape(QFrame::HLine);

        gridLayout_25->addWidget(line_13, 26, 0, 1, 1);

        pushButton_11 = new QPushButton(scrollAreaWidgetContents_5);
        pushButton_11->setObjectName(QString::fromUtf8("pushButton_11"));

        gridLayout_25->addWidget(pushButton_11, 25, 0, 1, 1);

        line_2 = new QFrame(scrollAreaWidgetContents_5);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShadow(QFrame::Plain);
        line_2->setLineWidth(5);
        line_2->setFrameShape(QFrame::HLine);

        gridLayout_25->addWidget(line_2, 0, 0, 1, 1);

        spinBox_huigu = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_huigu->setObjectName(QString::fromUtf8("spinBox_huigu"));
        spinBox_huigu->setMinimum(3);
        spinBox_huigu->setMaximum(20);
        spinBox_huigu->setValue(8);

        gridLayout_25->addWidget(spinBox_huigu, 8, 0, 1, 1);

        label_2 = new QLabel(scrollAreaWidgetContents_5);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setTextFormat(Qt::RichText);
        label_2->setAlignment(Qt::AlignCenter);

        gridLayout_25->addWidget(label_2, 21, 0, 1, 1);

        line_9 = new QFrame(scrollAreaWidgetContents_5);
        line_9->setObjectName(QString::fromUtf8("line_9"));
        line_9->setFrameShadow(QFrame::Plain);
        line_9->setLineWidth(5);
        line_9->setFrameShape(QFrame::HLine);

        gridLayout_25->addWidget(line_9, 28, 0, 1, 1);

        pushButton_4 = new QPushButton(scrollAreaWidgetContents_5);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));

        gridLayout_25->addWidget(pushButton_4, 31, 0, 1, 1);

        line_3 = new QFrame(scrollAreaWidgetContents_5);
        line_3->setObjectName(QString::fromUtf8("line_3"));
        line_3->setFrameShadow(QFrame::Plain);
        line_3->setLineWidth(5);
        line_3->setFrameShape(QFrame::HLine);

        gridLayout_25->addWidget(line_3, 2, 0, 1, 1);

        label_10 = new QLabel(scrollAreaWidgetContents_5);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout_25->addWidget(label_10, 29, 0, 1, 1);

        spinBox_bofang_liebao = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_bofang_liebao->setObjectName(QString::fromUtf8("spinBox_bofang_liebao"));
        spinBox_bofang_liebao->setMinimum(600);
        spinBox_bofang_liebao->setMaximum(999999);
        spinBox_bofang_liebao->setValue(1500);

        gridLayout_25->addWidget(spinBox_bofang_liebao, 19, 0, 1, 1);

        checkBox_dongtai_close = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_dongtai_close->setObjectName(QString::fromUtf8("checkBox_dongtai_close"));

        gridLayout_25->addWidget(checkBox_dongtai_close, 16, 0, 1, 1);

        line_5 = new QFrame(scrollAreaWidgetContents_5);
        line_5->setObjectName(QString::fromUtf8("line_5"));
        line_5->setFrameShape(QFrame::HLine);
        line_5->setFrameShadow(QFrame::Sunken);

        gridLayout_25->addWidget(line_5, 6, 0, 1, 1);

        line_16 = new QFrame(scrollAreaWidgetContents_5);
        line_16->setObjectName(QString::fromUtf8("line_16"));
        line_16->setFrameShape(QFrame::HLine);
        line_16->setFrameShadow(QFrame::Sunken);

        gridLayout_25->addWidget(line_16, 17, 0, 1, 1);

        scrollArea_5->setWidget(scrollAreaWidgetContents_5);

        gridLayout_8->addWidget(scrollArea_5, 0, 0, 1, 1);

        tabWidget->addTab(tab_2, QString());
        tab_10 = new QWidget();
        tab_10->setObjectName(QString::fromUtf8("tab_10"));
        gridLayout_16 = new QGridLayout(tab_10);
        gridLayout_16->setObjectName(QString::fromUtf8("gridLayout_16"));
        label_6 = new QLabel(tab_10);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(label_6, 0, 0, 1, 1);

        textEdit = new QTextEdit(tab_10);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setLineWrapMode(QTextEdit::WidgetWidth);

        gridLayout_16->addWidget(textEdit, 1, 0, 1, 1);

        frame_2 = new QFrame(tab_10);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        gridLayout_15 = new QGridLayout(frame_2);
        gridLayout_15->setObjectName(QString::fromUtf8("gridLayout_15"));
        pushButton_textedit_celer = new QPushButton(frame_2);
        pushButton_textedit_celer->setObjectName(QString::fromUtf8("pushButton_textedit_celer"));

        gridLayout_15->addWidget(pushButton_textedit_celer, 0, 0, 1, 1);

        pushButton_dele_jushuh = new QPushButton(frame_2);
        pushButton_dele_jushuh->setObjectName(QString::fromUtf8("pushButton_dele_jushuh"));

        gridLayout_15->addWidget(pushButton_dele_jushuh, 0, 1, 1, 2);

        pushButton_dele_oushuh = new QPushButton(frame_2);
        pushButton_dele_oushuh->setObjectName(QString::fromUtf8("pushButton_dele_oushuh"));

        gridLayout_15->addWidget(pushButton_dele_oushuh, 0, 3, 1, 1);

        pushButton_6 = new QPushButton(frame_2);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));

        gridLayout_15->addWidget(pushButton_6, 1, 2, 1, 2);

        pushButton_daoru_text = new QPushButton(frame_2);
        pushButton_daoru_text->setObjectName(QString::fromUtf8("pushButton_daoru_text"));

        gridLayout_15->addWidget(pushButton_daoru_text, 1, 0, 1, 2);


        gridLayout_16->addWidget(frame_2, 2, 0, 1, 1);

        tabWidget->addTab(tab_10, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QString::fromUtf8("tab_5"));
        gridLayout_18 = new QGridLayout(tab_5);
        gridLayout_18->setObjectName(QString::fromUtf8("gridLayout_18"));
        scrollArea = new QScrollArea(tab_5);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setStyleSheet(QString::fromUtf8(""));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 461, 1092));
        gridLayout_17 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_17->setObjectName(QString::fromUtf8("gridLayout_17"));
        widget = new QWidget(scrollAreaWidgetContents);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setStyleSheet(QString::fromUtf8(""));
        gridLayout_7 = new QGridLayout(widget);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        textBrowser_2 = new QTextBrowser(widget);
        textBrowser_2->setObjectName(QString::fromUtf8("textBrowser_2"));

        gridLayout_7->addWidget(textBrowser_2, 0, 0, 1, 1);

        label_datapath = new QLabel(widget);
        label_datapath->setObjectName(QString::fromUtf8("label_datapath"));
        label_datapath->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_datapath, 1, 0, 1, 1);

        label_7 = new QLabel(widget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_7, 2, 0, 1, 1);

        pushButton_8 = new QPushButton(widget);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));

        gridLayout_7->addWidget(pushButton_8, 3, 0, 1, 1);


        gridLayout_17->addWidget(widget, 0, 0, 1, 1);

        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout_18->addWidget(scrollArea, 0, 0, 1, 1);

        tabWidget->addTab(tab_5, QString());

        gridLayout->addWidget(tabWidget, 2, 0, 1, 1);

        label_show = new QLabel(Widget);
        label_show->setObjectName(QString::fromUtf8("label_show"));
        QPalette palette;
        QBrush brush(QColor(255, 255, 225, 80));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Active, QPalette::Window, brush);
        QBrush brush1(QColor(255, 255, 0, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush1);
        label_show->setPalette(palette);
        QFont font4;
        font4.setFamily(QString::fromUtf8("Microsoft YaHei UI"));
        font4.setPointSize(9);
        font4.setBold(false);
        label_show->setFont(font4);
        label_show->setStyleSheet(QString::fromUtf8("label_show{rgb(255, 0, 0)}"));

        gridLayout->addWidget(label_show, 0, 0, 1, 1);

        line_11 = new QFrame(Widget);
        line_11->setObjectName(QString::fromUtf8("line_11"));
        QFont font5;
        font5.setFamily(QString::fromUtf8("\346\245\267\344\275\223"));
        font5.setPointSize(12);
        line_11->setFont(font5);
        line_11->setFrameShadow(QFrame::Plain);
        line_11->setFrameShape(QFrame::HLine);

        gridLayout->addWidget(line_11, 1, 0, 1, 1);


        retranslateUi(Widget);

        tabWidget->setCurrentIndex(0);
        tabWidget_2->setCurrentIndex(0);
        tabWidget_3->setCurrentIndex(0);
        pushButton->setDefault(true);
        pushButton_last->setDefault(false);


        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QCoreApplication::translate("Widget", "Widget", nullptr));
        label->setText(QCoreApplication::translate("Widget", "\345\275\223\345\211\215\345\215\225\350\257\215", nullptr));
        label_dongtai_word->setText(QCoreApplication::translate("Widget", "\345\212\250\346\200\201\345\215\225\350\257\215", nullptr));
        label_yingjie_huafen->setText(QCoreApplication::translate("Widget", "\351\237\263\350\212\202\345\210\222\345\210\206", nullptr));
        pushButton_change_pic1->setText(QCoreApplication::translate("Widget", "\346\215\242\345\274\240\345\233\276\347\211\207", nullptr));
        label_pic1->setText(QCoreApplication::translate("Widget", "\347\224\250\345\233\276\347\211\207\345\270\256\345\212\251\350\201\224\346\203\263", nullptr));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_3), QCoreApplication::translate("Widget", "\345\233\276\347\211\207\350\256\260\345\277\2061", nullptr));
        label_pic2->setText(QCoreApplication::translate("Widget", "\347\224\250\345\233\276\347\211\207\345\270\256\345\212\251\350\201\224\346\203\263", nullptr));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_4), QCoreApplication::translate("Widget", "\345\233\276\347\211\207\350\256\260\345\277\2062", nullptr));
        pushButton_3->setText(QCoreApplication::translate("Widget", "\346\237\245\347\234\213\345\275\223\345\211\215\345\215\225\350\257\215\345\210\227\350\241\250\347\232\204\347\277\273\350\257\221(\346\234\211\351\201\223)", nullptr));
        pushButton_10->setText(QCoreApplication::translate("Widget", "\344\277\235\345\255\230\345\210\260\350\257\215\345\205\270\346\234\254", nullptr));
        pushButton_14->setText(QCoreApplication::translate("Widget", "\346\250\241\347\263\212\346\237\245\346\211\276\345\275\223\345\211\215\345\215\225\350\257\215", nullptr));
        pushButton_9->setText(QCoreApplication::translate("Widget", "\346\237\245\350\257\242", nullptr));
        checkBox_mohu->setText(QCoreApplication::translate("Widget", "\345\274\200\345\220\257\346\250\241\347\263\212\346\237\245\346\211\276", nullptr));
        label_11->setText(QCoreApplication::translate("Widget", "\346\237\245\350\257\215\345\205\270(\350\213\261->\346\261\211)(\346\261\211->\351\237\263)", nullptr));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_9), QCoreApplication::translate("Widget", "\345\234\250\347\272\277\350\257\215\345\205\270", nullptr));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_6), QCoreApplication::translate("Widget", "\345\215\225\350\257\215\350\241\250", nullptr));
#if QT_CONFIG(tooltip)
        listView_huigu->setToolTip(QCoreApplication::translate("Widget", "<html><head/><body><p>\345\217\214\345\207\273 \347\277\273\350\257\221\346\241\206\345\257\271\345\272\224\351\207\212\346\204\217 \346\267\273\345\212\240\350\257\245\345\215\225\350\257\215\345\210\260\347\224\237\350\257\215\346\234\254</p><p>\345\215\225\345\207\273\346\222\255\346\224\276\351\237\263\351\242\221</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        listView_huigu_shiyi->setToolTip(QCoreApplication::translate("Widget", "<html><head/><body><p>\345\217\214\345\207\273 \345\215\225\350\257\215\351\207\212\346\204\217 \346\267\273\345\212\240\345\257\271\345\272\224\345\215\225\350\257\215\345\210\260\347\224\237\350\257\215\346\234\254</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        pushButton_lookshiyi->setText(QCoreApplication::translate("Widget", "\346\237\245\347\234\213/\351\232\220\350\227\217 \345\215\225\350\257\215\351\207\212\346\204\217", nullptr));
        pushButton_15->setText(QCoreApplication::translate("Widget", "\345\274\200\345\247\213\346\265\213\350\257\225", nullptr));
        tabWidget_3->setTabText(tabWidget_3->indexOf(tab_13), QCoreApplication::translate("Widget", "\345\233\236\351\241\276", nullptr));
        textBrowser_cheshi->setHtml(QCoreApplication::translate("Widget", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'\346\245\267\344\275\223'; font-size:14pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\347\255\224(\345\257\271/\351\224\231)\344\272\206!</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">china</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\344\270\255\345\233\275</p></body></html>", nullptr));
        label_cheshi_pic->setText(QCoreApplication::translate("Widget", "\345\233\276\347\211\207\350\256\260\345\277\206", nullptr));
        label_danchi_cheshi->setText(QCoreApplication::translate("Widget", "china", nullptr));
        radioButton_4->setText(QCoreApplication::translate("Widget", "\351\200\211\351\241\271", nullptr));
        radioButton_3->setText(QCoreApplication::translate("Widget", "\351\200\211\351\241\271", nullptr));
        radioButton_2->setText(QCoreApplication::translate("Widget", "\351\200\211\351\241\271", nullptr));
        radioButton->setText(QCoreApplication::translate("Widget", "\351\200\211\351\241\271", nullptr));
        tabWidget_3->setTabText(tabWidget_3->indexOf(tab_14), QCoreApplication::translate("Widget", "\346\265\213\350\257\225", nullptr));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_8), QCoreApplication::translate("Widget", "\345\215\225\350\257\215\345\233\236\351\241\276", nullptr));
        pushButton_13->setText(QCoreApplication::translate("Widget", "\346\237\245\347\234\213\351\232\220\350\227\217\345\215\225\350\257\215", nullptr));
        checkBox_stop_xunhuan_currlist->setText(QCoreApplication::translate("Widget", "\346\232\202\345\201\234\346\222\255\346\224\276\345\210\227\350\241\250", nullptr));
        pushButton_16->setText(QCoreApplication::translate("Widget", "\345\276\252\347\216\257\346\222\255\346\224\276\345\210\227\350\241\250", nullptr));
        pushButton_12->setText(QCoreApplication::translate("Widget", "\346\237\245\347\234\213\351\232\220\350\227\217\345\215\225\350\257\215\351\207\212\346\204\217", nullptr));
        pushButton->setText(QCoreApplication::translate("Widget", "\345\274\200\345\247\213/\346\232\202\345\201\234\346\222\255\346\224\276", nullptr));
        pushButton_last->setText(QCoreApplication::translate("Widget", "\344\270\212\344\270\200\344\270\252", nullptr));
        pushButton_wordslist->setText(QCoreApplication::translate("Widget", "\345\215\225\350\257\215\345\210\227\350\241\250", nullptr));
        pushButton_next->setText(QCoreApplication::translate("Widget", "\344\270\213\344\270\200\344\270\252", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("Widget", "\350\256\260\345\215\225\350\257\215", nullptr));
        label_13->setText(QCoreApplication::translate("Widget", "\350\256\276\347\275\256\346\226\260\347\232\204\346\226\207\344\273\266\345\220\215\n"
"(\344\270\215\350\246\201\345\214\205\345\220\253\347\211\271\346\256\212\345\255\227\347\254\246[ / : * ? \" < > | ])", nullptr));
        label_5->setText(QCoreApplication::translate("Widget", "\345\216\206\345\217\262\346\225\260\346\215\256", nullptr));
        pushButton_2->setText(QCoreApplication::translate("Widget", "\345\257\274\345\205\245,\347\274\226\350\276\221\345\216\206\345\217\262\346\225\260\346\215\256(\345\267\262\345\220\210\345\271\266)", nullptr));
#if QT_CONFIG(tooltip)
        listView_lishi_shuju->setToolTip(QCoreApplication::translate("Widget", "<html><head/><body><p>\345\217\214\345\207\273\350\277\233\350\241\214\346\233\264\345\244\232\346\223\215\344\275\234</p><p><br/></p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        tabWidget->setTabText(tabWidget->indexOf(tab_7), QCoreApplication::translate("Widget", "\345\257\274\345\205\245\345\216\206\345\217\262\346\225\260\346\215\256", nullptr));
        label_12->setText(QCoreApplication::translate("Widget", "\345\212\250\346\200\201\345\215\225\350\257\215(\346\257\253\347\247\222);\345\215\225\350\257\215\346\222\255\346\224\276\351\227\264\351\232\224(\346\257\253\347\247\222)", nullptr));
        openfile_bytxt->setText(QCoreApplication::translate("Widget", "\345\257\274\345\205\245\345\216\206\345\217\262app\346\225\260\346\215\256", nullptr));
        pushButton_5->setText(QCoreApplication::translate("Widget", "\346\270\205\351\231\244\345\233\276\347\211\207\347\274\223\345\255\230", nullptr));
        label_14->setText(QCoreApplication::translate("Widget", "\345\276\252\347\216\257\346\222\255\346\224\276\345\210\227\350\241\250(\346\257\253\347\247\222)", nullptr));
        label_3->setText(QCoreApplication::translate("Widget", "\350\256\276\347\275\256", nullptr));
        label_8->setText(QCoreApplication::translate("Widget", "\345\215\225\350\257\215\346\265\213\350\257\225\351\200\211\351\241\271\345\207\206\347\241\256\345\272\246", nullptr));
        pushButton_7->setText(QCoreApplication::translate("Widget", "\345\257\274\345\207\272\346\211\200\346\234\211\346\225\260\346\215\256", nullptr));
        label_4->setText(QCoreApplication::translate("Widget", "\345\215\225\350\257\215\351\227\264\351\232\224\346\222\255\346\224\276(\346\257\253\347\247\222):", nullptr));
        checkBox_xunhuan->setText(QCoreApplication::translate("Widget", "\345\276\252\347\216\257\346\222\255\346\224\276(\345\217\226\346\266\210\345\210\231\351\227\264\351\232\224\346\222\255\346\224\276)", nullptr));
        pushButton_chengchiben->setText(QCoreApplication::translate("Widget", "\345\257\274\345\205\245,\347\274\226\350\276\221\347\224\237\350\257\215\346\234\254", nullptr));
        label_9->setText(QCoreApplication::translate("Widget", "\345\205\266\344\273\226", nullptr));
        checkBox_huigu->setText(QCoreApplication::translate("Widget", "\345\215\225\350\257\215\345\233\236\351\241\276", nullptr));
        pushButton_11->setText(QCoreApplication::translate("Widget", "\345\257\274\345\205\245,\347\274\226\350\276\221\350\257\215\345\205\270\346\234\254", nullptr));
        label_2->setText(QCoreApplication::translate("Widget", "\345\257\274\345\205\245\345\215\225\350\257\215", nullptr));
        pushButton_4->setText(QCoreApplication::translate("Widget", "\351\200\211\346\213\251\350\203\214\346\231\257\345\233\276", nullptr));
        label_10->setText(QCoreApplication::translate("Widget", "\350\203\214\346\231\257\351\200\217\346\230\216\345\272\246", nullptr));
        checkBox_dongtai_close->setText(QCoreApplication::translate("Widget", "\345\205\263\351\227\255\345\212\250\346\200\201\345\215\225\350\257\215", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("Widget", "\351\200\211\351\241\271", nullptr));
        label_6->setText(QCoreApplication::translate("Widget", "\346\226\207\346\234\254\346\241\206(\344\273\2160\350\241\214\345\274\200\345\247\213)", nullptr));
        pushButton_textedit_celer->setText(QCoreApplication::translate("Widget", "\346\270\205\351\231\244\346\226\207\346\234\254", nullptr));
        pushButton_dele_jushuh->setText(QCoreApplication::translate("Widget", "\345\210\240\351\231\244\345\245\207\346\225\260\350\241\214", nullptr));
        pushButton_dele_oushuh->setText(QCoreApplication::translate("Widget", "\345\210\240\351\231\244\345\201\266\346\225\260\350\241\214", nullptr));
        pushButton_6->setText(QCoreApplication::translate("Widget", "\347\241\256\345\256\232\344\277\235\345\255\230", nullptr));
        pushButton_daoru_text->setText(QCoreApplication::translate("Widget", "\345\257\274\345\205\245\346\226\207\346\234\254\345\210\260\345\215\225\350\257\215\350\241\250", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_10), QCoreApplication::translate("Widget", "\346\226\207\346\234\254\345\244\204\347\220\206\345\267\245\345\205\267", nullptr));
        textBrowser_2->setHtml(QCoreApplication::translate("Widget", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'\346\245\267\344\275\223'; font-size:14pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\345\270\256\345\212\251:</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\342\200\234\344\275\231\345\260\235\350\260\223\357\274\232</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\350\257\273\344\271\246\346\234\211\344\270\211\345\210\260\357\274\214\350\260\223\345\277\203\345\210\260\343\200\201\347\234\274\345\210\260\343\200\201\345\217\243\345\210\260\343"
                        "\200\202</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\345\277\203\344\270\215\345\234\250\346\255\244\357\274\214\345\210\231\347\234\274\344\270\215\347\234\213\344\273\224\347\273\206\357\274\214\345\277\203\347\234\274\346\227\242\344\270\215\344\270\223\344\270\200\357\274\214\345\215\264\345\217\252\346\274\253\346\265\252\350\257\265\350\257\273\357\274\214\345\206\263\344\270\215\350\203\275\350\256\260\357\274\214\350\256\260\344\272\246\344\270\215\350\203\275\344\271\205\344\271\237\343\200\202</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\344\270\211\345\210\260\344\271\213\344\270\255\357\274\214\345\277\203\345\210\260\346\234\200\346\200\245\357\274\214\345\277\203\346\227\242\345\210\260\347\237\243\357\274\214\347\234\274\345\217\243\345\262\202\344\270\215\345\210\260\344\271\216?\342\200\235</p>\n"
"<p style=\"-qt-paragraph-ty"
                        "pe:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\344\270\200.\350\275\257\344\273\266\347\211\271\347\202\271:</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">1.[\350\200\263\345\210\260] \346\264\227\350\204\221\346\222\255\346\224\276\351\237\263\351\242\221</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">2.[\347\234\274\345\210\260] \345\233\276\347\211\207\345\270\256\345\212\251\350\201\224\346\203\263</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">3.[\345\233\236\351\241\276] \345\256\232\351\207\217\345\233\236\351\241\276\345\215\225\350\257\215</p>\n"
"<p sty"
                        "le=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\344\272\214.\345\237\272\347\241\200\345\212\237\350\203\275:</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">1.\345\215\225\350\257\215\346\225\260\346\215\256\344\277\235\345\255\230 [\345\220\210\345\271\266\344\277\235\345\255\230\347\211\210\346\234\254] [\345\210\206\346\254\241\344\277\235\345\255\230\347\211\210\346\234\254]</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">2.\347\224\237\350\257\215\346\234\254 \350\257\215\345\205\270\346\234\254</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\""
                        ">3.\344\273\245\344\270\212\346\225\260\346\215\256\345\235\207\345\217\257 [\347\274\226\350\276\221\345\271\266\351\207\215\346\226\260\344\277\235\345\255\230] [\345\217\257\345\257\274\345\205\245\345\215\225\350\257\215\350\241\250\350\277\233\350\241\214\350\256\260\345\277\206]</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">4.\346\211\200\346\234\211\346\225\260\346\215\256\345\235\207\345\217\257\345\257\274\345\207\272,\345\271\266\345\217\257\344\273\245\345\206\215\346\254\241\345\257\274\345\205\245app</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">5.\350\257\215\345\205\270\345\212\237\350\203\275:\345\217\257\344\273\245 [\345\215\225\350\257\215\346\237\245\346\261\211\350\257"
                        "\255] [\346\261\211\350\257\255\346\237\245\345\215\225\350\257\215] [\345\217\257\344\277\235\345\255\230\350\257\215\345\205\270\346\234\254]</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">6.\345\217\257\344\273\245\345\234\250\347\272\277\346\237\245\345\215\225\350\257\215\345\210\227\350\241\250\344\270\255 \345\275\223\345\211\215\351\200\211\344\270\255\347\232\204\345\215\225\350\257\215(\346\234\211\351\201\223)</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\344\270\211.\347\225\214\351\235\242:</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">1.\345\217\257\351\200\211\344\270\211\345\274\240\350\203\214"
                        "\346\231\257\345\233\276</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">2.\345\217\257\350\260\203\350\203\214\346\231\257\351\200\217\346\230\216\345\272\246</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\345\233\233.\350\257\264\346\230\216:</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">1.[\345\215\225\350\257\215\345\233\236\351\241\276\345\244\204] </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\345\217\214\345\207\273 \345\215\225\350\257\215\351\207\212\346\204\217 \346\267\273\345\212\240\345\257\271\345\272\224\345\215\225"
                        "\350\257\215\345\210\260\347\224\237\350\257\215\346\234\254</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\345\215\225\345\207\273 \345\215\225\350\257\215 \346\222\255\346\224\276\351\237\263\351\242\221</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">2.[\345\257\274\345\205\245\345\216\206\345\217\262\346\225\260\346\215\256] </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\345\217\214\345\207\273 \350\277\233\350\241\214\350\241\214\346\233\264\345\244\232\346\223\215\344\275\234</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">"
                        "\344\272\224.\345\273\272\350\256\256:</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">1.\347\224\250\345\277\203\345\216\273\350\256\260</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">2.\347\224\261\350\213\261\350\257\255\345\215\225\350\257\215\350\256\260\346\261\211\350\257\255 [\351\230\205\350\257\273\345\245\275]</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">3.\347\224\261\346\261\211\350\257\255\350\201\224\346\203\263\350\213\261\350\257\255 \347\224\261\347\224\237\346\264\273\344\272\213\347\211\251\350\201\224\346\203\263\350\213\261\350\257\255 [\345\206\231\344\275\234\345\245\275] [\347\277\273\350\257\221\345\245\275]</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">4.\345\244"
                        "\232\345\220\254\345\215\225\350\257\215 [\345\220\254\345\212\233\345\245\275] \347\273\223\345\220\210\345\217\221\351\237\263\350\256\260\345\215\225\350\257\215[\350\256\260\345\276\227\345\277\253]</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">5.\347\273\223\345\220\210\345\233\236\351\241\276\345\212\237\350\203\275 \345\244\232\345\244\232\345\233\236\351\241\276 [\350\256\260\345\276\227\346\233\264\347\211\242]</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">6.\346\257\217\345\244\251\345\256\232\351\207\217 \345\256\232\346\227\266\350\256\260\345\215\225\350\257\215</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-i"
                        "ndent:0px;\">7.\344\270\200\345\256\232\350\246\201\345\244\232\345\244\232\345\233\236\351\241\276 \344\270\200\345\244\251\344\270\244\344\270\211\351\201\215 </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">8.\344\270\200\344\270\252\345\215\225\350\257\215\344\271\237\344\270\215\350\246\201\350\212\261\345\244\252\351\225\277\346\227\266\351\227\264\345\216\273\350\256\260</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">9.\350\256\260\345\215\225\350\257\215\346\227\266 \350\204\221\346\265\267\351\207\214\350\257\225\347\235\200&quot;\345\206\231\345\207\272&quot;\350\277\231\344\270\252\345\215\225\350\257\215</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">10.\345\210\222\345\210\206\345\215\225\350\257\215\350\256\260\345\277\206</p>\n"
"<p style=\"-qt-paragraph-"
                        "type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\345\205\255.B\347\253\231\346\233\264\345\244\232\345\255\246\344\271\240\350\247\206\351\242\221</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">1.<a href=\"https://www.bilibili.com/video/BV18R4y1F7Zf?spm_id_from=333.999.0.0\"><span style=\" text-decoration: underline; color:#0000ff;\">\343\200\220\345\205\255\347\272\247\345\220\254\345\212\233\345\216\206\345\271\264\345\220\210\351\233\206\343\200\2212021.12.2_\345\223\224\345\223\251\345\223\224\345\223\251_bilibili</span></a></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a href=\"https://www.bilibili.com/video/BV18R4y1F7Zf?spm_id_from=333."
                        "999.0.0\"><span style=\" text-decoration: underline; color:#0000ff;\">2.</span></a><a href=\"https://www.bilibili.com/video/BV1cZ4y1d7kV?spm_id_from=333.999.0.0\"><span style=\" text-decoration: underline; color:#0000ff;\">CET6 \345\205\250\345\233\275\345\205\255\347\272\247\345\220\254\345\212\233\350\200\203\350\257\225\357\274\210\344\270\255\350\213\261\345\217\214\345\255\227\345\271\225\357\274\211\357\274\210\347\277\273\350\257\221\357\274\211_\345\223\224\345\223\251\345\223\224\345\223\251_bilibili</span></a></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">3.<a href=\"https://www.bilibili.com/video/BV1na411y7r2?spm_id_from=333.999.0.0\"><span style=\" text-decoration: underline; color:#0000ff;\">CET4 \345\205\250\345\233\275\345\244\247\345\255\246\350\213\261\350"
                        "\257\255 \345\233\233\347\272\247\345\220\254\345\212\233\350\200\203\350\257\225(\344\270\255\350\213\261\345\217\214\345\255\227\345\271\225)(\347\277\273\350\257\221)(2015\345\271\264\350\207\263\344\273\212)_\345\223\224\345\223\251\345\223\224\345\223\251_bilibili</span></a></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a href=\"https://www.bilibili.com/video/BV1na411y7r2?spm_id_from=333.999.0.0\"><span style=\" text-decoration: underline; color:#0000ff;\">4.</span></a><a href=\"https://www.bilibili.com/video/BV1AZ4y167LK?spm_id_from=333.999.0.0\"><span style=\" text-decoration: underline; color:#0000ff;\">\343\200\220\345\233\233\347\272\247\345\220\254\345\212\233\343\200\221\345\216\206\345\271\264\347\234\237\351\242\230 \345\244\247\345\220\210\351\233\206(\344\270\255\350\213\261\345\217\214\345\255\227\345\271\225:\347\277\273\350\257\221)(\344\274\230\345\214\226\347\211\210)_\345\223\224\345\223\251\345\223\224\345"
                        "\223\251_bilibili</span></a></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; text-decoration: underline; color:#0000ff;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a href=\"https://www.bilibili.com/video/BV1AZ4y167LK?spm_id_from=333.999.0.0\"><span style=\" text-decoration: underline; color:#0000ff;\">5.</span></a><a href=\"https://www.bilibili.com/video/BV1kS4y167tB?spm_id_from=333.999.0.0\"><span style=\" text-decoration: underline; color:#0000ff;\">TEM \350\213\261\350\257\255\344\270\223\345\205\253\345\220\254\345\212\233\347\234\237\351\242\230\357\274\2102009~2021\345\271\264\345\220\210\351\233\206) (\344\270\255\350\213\261\345\217\214\345\255\227\345\271\225)(\347\277\273\350\257\221)_\345\223\224\345\223\251\345\223\224\345\223\251_bilibili</span></a></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px"
                        "; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a href=\"https://www.bilibili.com/video/BV1kS4y167tB?spm_id_from=333.999.0.0\"><span style=\" text-decoration: underline; color:#0000ff;\">6.</span></a><a href=\"https://www.bilibili.com/video/BV1k34y1b7bk?spm_id_from=333.999.0.0\"><span style=\" text-decoration: underline; color:#0000ff;\">TEM4 \350\213\261\350\257\255\344\270\223\345\233\233 \345\220\254\345\212\233\347\234\237\351\242\230\357\274\2102009~2021\345\271\264\345\220\210\351\233\206) (\344\270\255\350\213\261\345\217\214\345\255\227\345\271\225)(\347\277\273\350\257\221)_\345\223\224\345\223\251\345\223\224\345\223\251_bilibili</span></a></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; text-decoration: underline; color:#0000ff;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a h"
                        "ref=\"https://www.bilibili.com/video/BV1k34y1b7bk?spm_id_from=333.999.0.0\"><span style=\" text-decoration: underline; color:#0000ff;\">7.</span></a><a href=\"https://www.bilibili.com/video/BV1aa411C7Wb?spm_id_from=333.999.0.0\"><span style=\" text-decoration: underline; color:#0000ff;\">\346\227\245\350\257\255N3\345\220\254\345\212\233\345\216\206\345\271\264\347\234\237\351\242\230(\345\217\214\345\255\227\345\271\225)(\347\277\273\350\257\221)_\345\223\224\345\223\251\345\223\224\345\223\251_bilibili</span></a></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a href=\"https://www.bilibili.com/video/BV1aa411C7Wb?spm_id_from=333.999.0.0\"><span style=\" text-decoration: underline; color:#0000ff;\">8.</span></a><a href=\"https://www.bilibili.com/video/BV1jT4y1Q7no?spm_id_from=333.999.0.0\"><span style=\" text-decoration: underline; color:#0000ff;\">[B\347\253\231\351\246\226\345\217\221] \347\273\217\345\205\270\345\275\261\350\247"
                        "\206\345\255\246\345\244\226\350\257\255 (\344\270\255\350\213\261\346\227\245\344\270\211\345\233\275\350\257\255\350\250\200)\347\273\203\344\271\240\345\217\243\350\257\255\345\222\214\345\220\254\345\212\233(\346\234\211\347\233\262\345\220\254\351\203\250\345\210\206)(\346\233\264\346\226\260ing)5_\345\223\224\345\223\251\345\223\224\345\223\251_bilibili</span></a></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; text-decoration: underline; color:#0000ff;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\344\270\203.\345\205\261\345\213\211:</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\345\212\240\346\262\271!\345\260\221\345\271\264!!!</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; m"
                        "argin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", nullptr));
        label_datapath->setText(QCoreApplication::translate("Widget", "\346\225\260\346\215\256\345\255\230\346\224\276\350\267\257\345\276\204", nullptr));
        label_7->setText(QCoreApplication::translate("Widget", "[\344\275\234\350\200\205:Maof]\350\201\224\347\263\273:2148334528@qq.com", nullptr));
        pushButton_8->setText(QCoreApplication::translate("Widget", "\344\272\206\350\247\243\346\233\264\345\244\232(csdn\357\274\214B\347\253\231)", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_5), QCoreApplication::translate("Widget", "\345\205\263\344\272\216", nullptr));
        label_show->setText(QCoreApplication::translate("Widget", "\350\276\223\345\207\272", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
